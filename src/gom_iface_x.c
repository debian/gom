/* gom, Gom is nOt yet another (Audio) Mixer.
 *
 * Copyright (C) 1996-2004  Stephan Sürken <absurd@olurdix.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * I originally used Hannu Savolainen's xvmixer as template for this
 * interface. I made *many* changes, however. Here goes his copyright notice:
 */

/* Copyright by Hannu Savolainen 1993
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer. 2.
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS `AS IS' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
#include "gom.h"

#ifdef GOM_ENABLE_X_GOMII

/*
 * gom_iface_x.c: x gomii
 */

/*
 * INCLUDES
 */
#include <xview/frame.h>
#include <xview/panel.h>

/*
 * MACROS
 */
#define GOM_IFACE_X_SCROLL_LINES 35
#define GOM_IFACE_X_LINE_HEIGHT 15

/*
 * DECLARATIONS/DEFINTIONS
 */
int gom_iface_x_is_initialized = 0;

Frame		gom_iface_x_frame;
Panel		gom_iface_x_panel;

Panel_item	gom_iface_x_label[GOM_DRIVER_CHANNELS];
Panel_item	gom_iface_x_volume[GOM_DRIVER_CHANNELS][GOM_DRIVER_CHANNEL_CHANNELS];
Panel_item	gom_iface_x_lock[GOM_DRIVER_CHANNELS];
Panel_item	gom_iface_x_rec_src[GOM_DRIVER_CHANNELS];
Panel_item      gom_iface_x_info_text, gom_iface_x_action_string;
Panel_item      gom_iface_x_action_button, gom_iface_x_help_button, gom_iface_x_quit_button;
Panel_item      gom_iface_x_mixer_message, gom_iface_x_last_refresh_message;

Frame		gom_iface_x_scroll_frame;
Panel		gom_iface_x_scroll_panel;
Panel_item      gom_iface_x_scroll_gadget[GOM_IFACE_X_SCROLL_LINES];
Panel_item      gom_iface_x_scroll_continue;

/* help, needed for scrolling */
int gom_iface_x_info_line = 0;

/* xv unique keys for button-constant gom action values */
int		gom_iface_x_action_action_key, gom_iface_x_action_c_key;
int             gom_iface_x_action_c_c_key;

/* all action parameters */
Panel_item gom_iface_x_action_xv_item;
int gom_iface_x_action_xv_value;
Event * gom_iface_x_action_xv_event;
int gom_iface_x_action_action;
int gom_iface_x_action_meta;

/* for K (lock all) */
int gom_iface_x_lock_flip = 0;

/* the completely unnecessary deja-vu thing */
int gom_iface_x_deja_vu = 0;

/*
 * FUNCTION PROTOTYPES
 */

void
gom_iface_x_closeup();
void
gom_iface_x_init();


/*****************/

/*
 * Some Helps
 */

void
gom_iface_x_center(Panel panel, Panel_item item)
{
  int p_width  = xv_get(panel, XV_WIDTH);
  int i_width  = xv_get(item, XV_WIDTH);
  xv_set(item,
	 XV_X, (p_width - i_width) / 2,
	 NULL);
}

void
gom_iface_x_flush_right(Panel panel, Panel_item item)
{
  int p_width  = xv_get(panel, XV_WIDTH);
  int i_width  = xv_get(item, XV_WIDTH);
  xv_set(item,
	 XV_X, p_width - i_width - 20, /* STATIC */
	 NULL);
}


void
gom_iface_x_scroll_show_page()
{
  window_fit(gom_iface_x_scroll_panel);
  window_fit(gom_iface_x_scroll_frame);
  gom_iface_x_center(gom_iface_x_scroll_panel, gom_iface_x_scroll_continue);
  xv_window_loop(gom_iface_x_scroll_frame);
}

/*
 * Custom gom_info()
 */
void
gom_iface_x_info(enum gom_info_types kind, char * fmt, va_list vargs)
{
  char line[255];

  vsnprintf(line, 255, fmt, vargs);

  if (gom_info_scroll())
    {
      if ((++gom_iface_x_info_line) >= GOM_IFACE_X_SCROLL_LINES)
	{
          gom_iface_x_scroll_show_page();
	  /* prepare gadgets for new display */
	  for (gom_iface_x_info_line=0; gom_iface_x_info_line < GOM_IFACE_X_SCROLL_LINES; ++gom_iface_x_info_line)
	    xv_set(gom_iface_x_scroll_gadget[gom_iface_x_info_line],
		   PANEL_LABEL_STRING, "",
		   NULL);
          gom_iface_x_info_line = 0;
	}
      xv_set(gom_iface_x_scroll_gadget[gom_iface_x_info_line],
	     PANEL_LABEL_STRING, line, NULL);
    }
  else
    {
      xv_set(gom_iface_x_info_text,
	     PANEL_VALUE, line,
	     NULL);
    }
}

/*
 * Custom option arg
 */
char *
gom_iface_x_get_option_arg(char * prompt)
{
  static char option_arg[100];
  char * string;
  unsigned volume;

  strcpy(option_arg, "");

  if (gom_iface_x_action_meta)
    {
      /* get all but the first char */
      string = (char *) xv_get(gom_iface_x_action_string, PANEL_VALUE);
      if (string)
	{
	  /* skip first char, and all following spaces */
	  do ++string; while (*string == ' ');

	  /* copy ... */
	  strncpy(option_arg, string, 100);
	}
    }
  else
    {
      switch (gom_iface_x_action_action)
	{
	case 'l':
	  /* 0x7f=..0111 1111 we only use the least significant 7 bits */
	  volume = gom_iface_x_action_xv_value & 0x7f;
	  snprintf(option_arg, 100, "%d", volume);
	  break;

	case 'r':
	  snprintf(option_arg, 100, "%d", !gom_driver_c_r(gom_mixer_c_current()));
	  break;

	case 'k':
	  snprintf(option_arg, 100, "%d", !gom_mixer_c_l(gom_mixer_c_current()));
	  break;

	default:
	  break;
	}
    }
  return option_arg;
}

void
gom_iface_x_refresh_channel (int c)
{
  int cc;

  /* set all volumes */
  for(cc=gom_driver_c_c_first(c); cc > -1; cc=gom_driver_c_c_next(c, cc, +1))
    xv_set(gom_iface_x_volume[c][cc], PANEL_VALUE, gom_driver_c_c_v(c, cc), NULL);

  /* set record */
  if (gom_driver_c_r(c) > -1)
    xv_set(gom_iface_x_rec_src[c],
	   PANEL_VALUE, gom_driver_c_r(c),
	   NULL);

  /* set lock */
  xv_set(gom_iface_x_lock[c],
	 PANEL_VALUE, gom_mixer_c_l(c),
	 NULL);
}

void
gom_iface_x_update(enum gom_gomii_refresh_stage stage,
		   int new_channel)
{
  int c;

  switch (stage)
    {
    case GOM_GOMII_REFRESH_INIT:
      gom_iface_x_closeup();
      gom_iface_x_init();

      /* Only a greeting (and we are very smart about it (which really isn't actually needed) ;) */
      gom_info(GOM_INFO_NORMAL,
	       "Welcome%sto the x gomii!",
	       gom_iface_x_deja_vu ? " back " : " ");

    case GOM_GOMII_REFRESH_PAGE:
      /* redraw headline + center it */
      xv_set(gom_iface_x_mixer_message,
	     PANEL_LABEL_STRING, gom_driver_d(), NULL);
      gom_iface_x_center(gom_iface_x_panel, gom_iface_x_mixer_message);

    case GOM_GOMII_REFRESH_CHANNELS:
      for(c=gom_driver_c_first(); c > -1; c=gom_driver_c_next(c, +1))
	gom_iface_x_refresh_channel(c);

      /* update last refresh time */
      xv_set(gom_iface_x_last_refresh_message,
	     PANEL_LABEL_STRING, gom_gomii_last_refresh_str(), NULL);
      gom_iface_x_flush_right(gom_iface_x_panel, gom_iface_x_last_refresh_message);
      break;

    case GOM_GOMII_REFRESH_CURRENT_CHANNEL:
      gom_iface_x_refresh_channel(gom_mixer_c_current());

    default:
      break;
    }

  if (stage == GOM_GOMII_REFRESH_INIT)
    xv_main_loop(gom_iface_x_frame);
}

void
gom_iface_x_refresh()
{
  gom_iface_x_update(GOM_GOMII_REFRESH_CHANNELS, gom_mixer_c_current());
}

void
gom_iface_x_scroll_return(Panel_item item, int value, Event * event)
{
  xv_window_return(gom_iface_x_scroll_frame);
}

void
gom_iface_x_scroll_create()
{
  int i;
  gom_iface_x_scroll_frame = (Frame) xv_create(XV_NULL, FRAME,
 					  FRAME_LABEL, "X gomii text scroller",

 					  XV_WIDTH, 200,
 					  XV_HEIGHT, 50,
 					  NULL);

  gom_iface_x_scroll_panel = (Panel) xv_create(gom_iface_x_scroll_frame, PANEL,
 					  PANEL_LAYOUT, PANEL_VERTICAL,
 					  PANEL_ITEM_Y_GAP, 0,
					  NULL);

  for (i=0; i < GOM_IFACE_X_SCROLL_LINES; ++i)
    gom_iface_x_scroll_gadget[i] = xv_create(gom_iface_x_scroll_panel, PANEL_MESSAGE,
					PANEL_LABEL_STRING, " ",
					NULL);
  xv_set(gom_iface_x_scroll_panel,
	 PANEL_ITEM_Y_GAP, 20,
	 NULL);

  gom_iface_x_scroll_continue =
    xv_create(gom_iface_x_scroll_panel, PANEL_BUTTON,
	      PANEL_NOTIFY_PROC, gom_iface_x_scroll_return,
	      PANEL_LABEL_STRING, "Continue",
	      NULL);
}

void
gom_iface_x_pre_scroll()
{
  gom_info(GOM_INFO_NORMAL, "X gomii text scroller");
  gom_info_scroll_set(1);

  /* stop auto-refresh */
  gom_gomii_refival_set(GOM_INFO_VERBOSE+1, gom_gomii_refival(), NULL);

  gom_iface_x_scroll_create();
  gom_iface_x_info_line = -1;
}

void
gom_iface_x_post_scroll()
{
  /* display the last scroll page */
  if (gom_iface_x_info_line > -1)
      gom_iface_x_scroll_show_page();

  xv_destroy_safe(gom_iface_x_scroll_frame);

  /* start auto-refresh */
  gom_gomii_refival_set(GOM_INFO_VERBOSE+1, gom_gomii_refival(), gom_iface_x_refresh);

  gom_info_scroll_set(0);
  gom_info(GOM_INFO_NORMAL, "Back from scrolling...");
}

void
gom_iface_x_action (Panel_item item, int value, Event * event)
{
  enum gom_gomii_refresh_stage stage = GOM_GOMII_REFRESH_INIT;
  int c, cc;

  /* all action parameters */
  gom_iface_x_action_xv_item = item;
  gom_iface_x_action_xv_value = value;
  gom_iface_x_action_xv_event = event;
  gom_iface_x_action_action = (int) xv_get(item, XV_KEY_DATA, gom_iface_x_action_action_key);

  /* if demanded, quietly set the current channel */
  c = (int) xv_get(item, XV_KEY_DATA, gom_iface_x_action_c_key);
  if (c > -1)
      gom_mixer_c_current_set(GOM_INFO_VERBOSE+1, c);

  /* if demanded, quietly set the current volume */
  cc = (int) xv_get(item, XV_KEY_DATA, gom_iface_x_action_c_c_key);
  if (cc > -1)
    gom_mixer_c_c_current_set(GOM_INFO_VERBOSE+1, cc);

  /* check if we were called by the action string gadget */
  if ((gom_iface_x_action_meta = ((gom_iface_x_action_xv_item == gom_iface_x_action_string)
				  || (gom_iface_x_action_xv_item == gom_iface_x_action_button))))
    {
      sscanf((char *) xv_get(gom_iface_x_action_string, PANEL_VALUE), "%c", (char *) &gom_iface_x_action_action);
    }

  switch (gom_iface_x_action_action)
    {
      /* x gomii specials */
    case 'q':
      gom_iface_x_closeup();
      stage = GOM_GOMII_REFRESH_NO;
      break;

    default:
      stage = gom_action(GOM_INFO_NORMAL,
			 gom_iface_x_action_action, 0,
			 gom_iface_x_get_option_arg,
			 gom_iface_x_pre_scroll, gom_iface_x_post_scroll);
      break;
    }
  gom_iface_x_update(stage, gom_mixer_c_current());
}

void
gom_iface_x_init()
{
  static char * help_text[]=
  {
    "",
    "Help on X gomii",
    "---------------",
    "",
    "  You can use all command line options (except the few command",
    "  line _only_ options) using the standard UN*X-like one-character",
    "  option syntax; type _one_ option (plus argument if needed)",
    "  into the \"action\" string gadget (without the leading hyphen,",
    "  e.g. \"G\" or \"l 90\"). <RETURN> in the gadget, or a click",
    "  on the \"action\" button, will execute the command.",
    "",
    "  X gomii specials:",
    "",
    "    ...      The (hopefully) intuitive behaviour of the xview",
    "             channel gadgets. A thusly manipulated channel and ",
    "             (in case) volume channel becomes current.",
    "    <Action> Execute the command line option in the \"action\"",
    "             string gadget.",
    "    <Help>   Mouse-shortcut for --help/-h option.",
    "    q<Quit>  Quit the X gomii.",
    NULL
  };

  /* helpers */
  int c, cc, n, x, y, y_tmp, x_tmp, cboxes_y, cboxes_x, y_base, x_base, y_gap, y_top;
  int ymax = 0;
  int avail = gom_driver_c_len();

  gom_iface_x_action_action_key = xv_unique_key();
  gom_iface_x_action_c_key = xv_unique_key();
  gom_iface_x_action_c_c_key = xv_unique_key();

  /* realize a x=1x3=y rectangle (y = 3*x) */
  for (cboxes_x = 1; ((cboxes_x * 3) * cboxes_x) < avail; cboxes_x++);
  cboxes_y = 3*cboxes_x;

  /* Maybe this is necessary for *something*, so I leave this as comment... */
  /* xv_init(XV_INIT_ARGC_PTR_ARGV, &argc, argv, NULL); */

  gom_iface_x_frame = (Frame) xv_create(XV_NULL, FRAME,
				   FRAME_LABEL, GOM_TITLE,
				   NULL);

  gom_iface_x_panel = (Panel) xv_create(gom_iface_x_frame, PANEL,
				   PANEL_LAYOUT, PANEL_HORIZONTAL,
				   NULL);

  y = 4; /* STATIC */
  x = 5; /* STATIC */

  gom_iface_x_last_refresh_message = xv_create(gom_iface_x_panel, PANEL_MESSAGE,
					  PANEL_LABEL_STRING, "No refresh yet.",
					  XV_X, x,
					  XV_Y, y,
					  NULL);

  /* will be centered later */
  gom_iface_x_mixer_message = xv_create(gom_iface_x_panel, PANEL_MESSAGE,
				   PANEL_LABEL_STRING, gom_driver_d(),
				   PANEL_LABEL_BOLD, TRUE,
				   XV_X, x,
				   XV_Y, y,
				   NULL);
  /* generate y_gap */
  y_gap = xv_get(gom_iface_x_mixer_message, XV_HEIGHT) / 2;

  /* Next line! + GAP! */
  y += xv_get(gom_iface_x_mixer_message, XV_HEIGHT) + 2*y_gap;
  y_top = y;

  n = 0;
  for(c=gom_driver_c_first(); c > -1; c=gom_driver_c_next(c, +1))
    {
      x_base = x;
      y_base = y;

      gom_iface_x_label[c] = xv_create (gom_iface_x_panel, PANEL_MESSAGE,
					PANEL_LABEL_STRING, gom_driver_c_name(c, 1),
					PANEL_LABEL_BOLD, TRUE,
					XV_Y, y,
					XV_X, x+58, /* STATIC */
					NULL);

      /* Next line! */
      y += xv_get(gom_iface_x_label[c], XV_HEIGHT);
      y_tmp = y;
      x_tmp = x;

      /* volume sliders */
      for(cc=gom_driver_c_c_first(c); cc > -1; cc=gom_driver_c_c_next(c, cc, +1))
	{
	  gom_iface_x_volume[c][cc] =
	    (Panel_item) xv_create(gom_iface_x_panel, PANEL_SLIDER,
				   XV_KEY_DATA, gom_iface_x_action_action_key, 'l',
				   XV_KEY_DATA, gom_iface_x_action_c_key, c,
				   XV_KEY_DATA, gom_iface_x_action_c_c_key, cc,
				   PANEL_NOTIFY_PROC, gom_iface_x_action,

				   PANEL_NOTIFY_LEVEL, PANEL_ALL,
				   PANEL_SHOW_RANGE, FALSE,
				   PANEL_SHOW_VALUE, TRUE,
				   PANEL_SLIDER_END_BOXES, TRUE,
				   PANEL_DIRECTION, PANEL_HORIZONTAL,
				   XV_Y, y,
				   XV_X, x,
				   PANEL_VALUE, gom_driver_c_c_v(c, cc),
				   NULL);
	  y += xv_get(gom_iface_x_volume[c][cc], XV_HEIGHT);
	  x_tmp = x+xv_get(gom_iface_x_volume[c][cc], XV_WIDTH);
	}
      y = y_tmp-10; /* STATIC */
      x = x_tmp+5; /* STATIC */

      /* rec_src buttons */
      if (gom_driver_c_r(c) > -1)
	{
	  gom_iface_x_rec_src[c] =
	    (Panel_item) xv_create(gom_iface_x_panel, PANEL_TOGGLE,
				   XV_KEY_DATA, gom_iface_x_action_action_key, 'r',
				   XV_KEY_DATA, gom_iface_x_action_c_key, c,
				   XV_KEY_DATA, gom_iface_x_action_c_c_key, -1,
				   PANEL_NOTIFY_PROC, gom_iface_x_action,

				   PANEL_CHOICE_STRINGS, "R", NULL,
				   XV_Y, y,
				   XV_X, x,
				   PANEL_VALUE, gom_driver_c_r(c),
				   NULL);
	  /* Next line! */
	  y += xv_get(gom_iface_x_rec_src[c], XV_HEIGHT);
	}
      else
	{
	  /* Next line! (let's say, there MUST be at least one channel ...) */
	  y += xv_get(gom_iface_x_volume[c][0], XV_HEIGHT);
	}

      /* lock button */
      gom_iface_x_lock[c] =
	(Panel_item) xv_create(gom_iface_x_panel, PANEL_TOGGLE,
			       XV_KEY_DATA, gom_iface_x_action_action_key, 'k',
			       XV_KEY_DATA, gom_iface_x_action_c_key, c,
			       XV_KEY_DATA, gom_iface_x_action_c_c_key, -1,
			       PANEL_NOTIFY_PROC, gom_iface_x_action,

			       PANEL_CHOICE_STRINGS, "L", NULL,
			       PANEL_LAYOUT, PANEL_VERTICAL,
			       XV_Y, y,
			       XV_X, x,
			       PANEL_VALUE, gom_mixer_c_l(c),
			       NULL);

      /* Next line! */
      y += xv_get(gom_iface_x_volume[c][0], XV_HEIGHT) + 5; /* STATIC */

      ymax = y > ymax ? y : ymax;
      /* setting x, y */
      ++n;
      if (n >= cboxes_y)
	{
	  x = (x_base + xv_get(gom_iface_x_volume[c][0], XV_WIDTH)
	       + xv_get(gom_iface_x_lock[c], XV_WIDTH) + 20); /* STATIC */
	  y = y_top;
	  n = 0;
	}
      else
	{
	  /* GAP to next channel */
	  y += y_gap;
	  x = x_base;
	}
    }
  /* set window/panel size according to channel gadgets */
  window_fit(gom_iface_x_panel);
  window_fit(gom_iface_x_frame);

  gom_iface_x_center(gom_iface_x_panel, gom_iface_x_mixer_message);

  /* create remaining gadgets */
  x = 4; /* STATIC */
  y = ymax+3*y_gap;

  gom_iface_x_info_text = xv_create(gom_iface_x_panel, PANEL_TEXT,
			       PANEL_LABEL_STRING, "Info",
			       PANEL_VALUE, "",
			       PANEL_READ_ONLY, TRUE,
			       /* roughly two lines */
			       PANEL_VALUE_STORED_LENGTH, 160,
			       /* try to pan the info across all window */
 			       PANEL_VALUE_DISPLAY_LENGTH, 50,   /* STATIC */
			       XV_Y, y,
			       XV_X, x,
			       NULL);

  /* Next line! */
  y += xv_get(gom_iface_x_info_text, XV_HEIGHT) + y_gap;
  x = 4; /* STATIC */

  gom_iface_x_action_button =
    xv_create(gom_iface_x_panel, PANEL_BUTTON,
	      XV_KEY_DATA, gom_iface_x_action_c_key, -1,
	      XV_KEY_DATA, gom_iface_x_action_c_c_key, -1,
	      PANEL_NOTIFY_PROC, gom_iface_x_action,
	      XV_Y, y,
	      XV_X, x,
	      PANEL_LABEL_STRING, "Action",
	      NULL);

  gom_iface_x_action_string =
    xv_create(gom_iface_x_panel, PANEL_TEXT,
	      XV_KEY_DATA, gom_iface_x_action_c_key, -1,
	      XV_KEY_DATA, gom_iface_x_action_c_c_key, -1,
	      PANEL_NOTIFY_PROC, gom_iface_x_action,
	      /* 				   PANEL_LABEL_STRING, "Action", */
	      PANEL_VALUE_DISPLAY_LENGTH, 20,
	      XV_Y, y,
	      XV_X, (x += xv_get(gom_iface_x_action_button, XV_WIDTH)+5), /* STATIC */
	      NULL);

  gom_iface_x_help_button =
    xv_create(gom_iface_x_panel, PANEL_BUTTON,
	      XV_KEY_DATA, gom_iface_x_action_action_key, 'h',
	      XV_KEY_DATA, gom_iface_x_action_c_key, -1,
	      XV_KEY_DATA, gom_iface_x_action_c_c_key, -1,

	      PANEL_NOTIFY_PROC, gom_iface_x_action,
	      XV_Y, y,
	      XV_X, (x += xv_get(gom_iface_x_action_string, XV_WIDTH)+5), /* STATIC */
	      PANEL_LABEL_STRING, "Help",
	      NULL);

  gom_iface_x_quit_button =
    xv_create(gom_iface_x_panel, PANEL_BUTTON,
	      XV_KEY_DATA, gom_iface_x_action_action_key, 'q',
	      XV_KEY_DATA, gom_iface_x_action_c_key, -1,
	      XV_KEY_DATA, gom_iface_x_action_c_c_key, -1,

	      PANEL_NOTIFY_PROC, gom_iface_x_action,
	      XV_Y, y,
	      XV_X, (x += xv_get(gom_iface_x_help_button, XV_WIDTH)),

	      PANEL_LABEL_STRING, "Quit",
	      NULL);

  gom_iface_x_flush_right(gom_iface_x_panel, gom_iface_x_quit_button);

  /* create final fitting panel & frame size */
  window_fit(gom_iface_x_panel);
  window_fit(gom_iface_x_frame);

  /* start auto-refresh */
  gom_gomii_refival_set(GOM_INFO_VERBOSE+1, gom_gomii_refival(), gom_iface_x_refresh);

  gom_info_custom_set(gom_iface_x_info);
  gom_action_help_special_set(help_text);

  gom_iface_x_is_initialized = 1;
}

void
gom_iface_x_closeup()
{
  if (gom_iface_x_is_initialized)
    {
      /* stop auto-refresh */
      gom_gomii_refival_set(GOM_INFO_VERBOSE+1, gom_gomii_refival(), NULL);

      gom_info_custom_set(NULL);
      gom_action_help_special_set(NULL);

      gom_iface_x_is_initialized = 0;

      xv_destroy_safe(gom_iface_x_frame);
    }
}

void
gom_iface_x()
{
  gom_iface_x_deja_vu = 1;

  /* pop up the window */
  gom_iface_x_update(GOM_GOMII_REFRESH_INIT, gom_mixer_c_current());
}

#else

/** ISO C forbids empty source files. */
int gom_iface_c_global_dummy = 0;

#endif
