/* gom, Gom is nOt yet another (Audio) Mixer.
 *
 * Copyright (C) 1996-2004  Stephan Sürken <absurd@olurdix.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * INCLUDES
 */
#include "gom.h"

/*
 * MACROS
 */

/*
 * DECLARATIONS/DEFINTIONS
 */

/*
 * FUNCTION PROTOTYPES
 */

int main (int argc, char * argv[])
{
	/* set defaults */
	gom_gomii_refival_set (GOM_INFO_VERBOSE+1, GOM_GOMII_REFIVAL_DEFAULT, gom_gomii_refproc_keep);
	gom_mixer_fadival_set (GOM_INFO_VERBOSE+1, GOM_MIXER_FADIVAL_DEFAULT);

	/* everything starts with the command line interface */
	gom_iface_c (argc, argv);

	/* closing the mixer (if one is open) */
	gom_driver_d_set (GOM_INFO_NORMAL, NULL);

	/* check the error count for final info & exit status */
	if (gom_info_errors > 0)
	{
		gom_info (GOM_INFO_VERBOSE, "%s%d error(s) detected while running.",
							/* "More than" would be false for one case, "More or equal than" would be too long */
							(gom_info_errors == INT_MAX) ? "At least " : "",
							gom_info_errors);
		/* warning */
		return 2;
	}
	else
	{
		/* there was definitely no failure */
		return 0;
	}
}
