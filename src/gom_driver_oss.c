/* gom, Gom is nOt yet another (Audio) Mixer.
 *
 * Copyright (C) 1996-2004  Stephan Sürken <absurd@olurdix.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * gom_driver_oss.c: interface to open sound system soundcard driver
 */

/* Shortcuts: C  OSS channel
 *            c  available channel
 *            v  volume
 *            r  record
 */

/*
 * INCLUDES
 */
#include "gom.h"

/*
 * MACROS
 */

/*
 * DECLARATIONS/DEFINTIONS
 */

/*
 * FUNCTION PROTOTYPES
 */


/************/

/*
 * MIXER SETTINGS
 */
struct
{
	/* Mixer life time constants */
	struct
	{
		int opened;                                /* TRUE if the mixer is successfully opened */
		char fn[GOM_DEVICE_FILE_SIZE];             /* mixer device _f_ile _n_ame */
		int fd;                                    /* mixer device _f_ile _d_escriptor */
		unsigned channelmask;                      /* OSS DEVMASK    (available? (i call them channels)) */
		unsigned stereomask;                       /* OSS STEREODEVS (stereo?) */
		unsigned recmask;                          /* OSS RECMASK    (recording available?) */
	} c;

	/* Mixer life time variables */
	struct
	{
		unsigned lockmask;
	} v;

	/* Mixer settings. Set by gom_set_*() functions */
	struct
	{
		int dummy; /** Not used, ISO C++ does not like empty structs. */
		/* unsigned recsrcmask;                stored in OSS driver */
		/* unsigned volume[GOM_CHANNELS];      stored in OSS driver */
	} s;

	/* Mixer settings snapshot */
	struct
	{
		unsigned recsrcmask;
		unsigned volume[GOM_DRIVER_CHANNELS];
	} z;
} gom_driver_oss = {{0, GOM_DEVICE_NOT_OPENED_TEXT, -1, 0, 0, 0}, /* gom_driver_oss.c.opened MUST be 0! */
										{~0},
										{0},
										{0}};

/*
 * LOCAL HELPERS
 */
unsigned gom_driver_private_getbits(unsigned x, int p, int n)  /* get n bits from x at position p (see K&R ;) */
{
	return (x >> (p+1-n)) & ~(~0 << n);
}

unsigned gom_driver_private_pow(unsigned base, int exp)  /* power for cardinals */
{
	int i;
	unsigned x=1;
	for (i = 0; i < exp; i++)
		x = x * base;
	return x;
}

unsigned gom_driver_private_setbit(unsigned x, int p, int on) /* set bit on/off at position p */
{
	if (on)
		return ( x | gom_driver_private_pow(2, p));
	else
		return (x & ~gom_driver_private_pow(2, p));
}

/*
 * THIS DRIVER'S ID
 */
char * gom_driver()
{
	static char name[] = "OSS (Open Sound System) API";
	return (char *) &name;
}

/*
 * DEVICE
 */

char * gom_driver_d()
{
	return (char *) &gom_driver_oss.c.fn;
}

int gom_driver_d_opened()
{
	return gom_driver_oss.c.opened;
}

int gom_driver_d_set(enum gom_info_types std_verb,
								 char * device_file)
{
#define GOM_DRIVER_BUG_DONNO "Maybe this indicates a misconfigured OSS sounddriver, some " \
                             "strange hardware bug, or some strange gom bug; but basically, " \
                             "I don't really have a clue."
	int new_mixer;
	unsigned channelmask;

	/* open the mixer device file */
	if (device_file == NULL) /* SPECIAL: ONLY closing device */
	{
		/* close the old mixer device, if opened */
		if (gom_driver_oss.c.opened)
		{
			close(gom_driver_oss.c.fd);
			strncpy(gom_driver_oss.c.fn, GOM_DEVICE_NOT_OPENED_TEXT, GOM_DEVICE_FILE_SIZE);
			gom_driver_oss.c.opened = 0;
		}
		return 1;
	}
	else if (gom_driver_oss.c.opened && strcmp(device_file, gom_driver_oss.c.fn) == 0)
	{
		/* Same mixer is already open */
		gom_info(std_verb,
						 "Mixer corresponding to special file %s is already open.",
						 device_file);
		return 1;
	}

	else if ((new_mixer = open(device_file, O_RDWR)) == -1)
	{
		/* Open error */
		gom_info(GOM_INFO_ERROR,
						 "Can't open mixer device special file \"%s\": %s.",
						 device_file, strerror(errno));
		gom_info_block(GOM_INFO_QUIET, 6,
									 "Error help: if loading a mixer special file leads to \"No such device\", "
									 "then you most likely do have the kernel sound driver not or incorrectly "
									 "installed. \"Permission denied\" means you need permissions by the local "
									 "admin in the first place.");

		return 0;
	}
	else if (ioctl(new_mixer, MIXER_READ(SOUND_MIXER_DEVMASK), &channelmask) == -1)
	{
		/* Strange error? */
		close(new_mixer);

		gom_info(GOM_INFO_ERROR, "I can't read the opened mixer special file %s.", device_file);
		gom_info_block(GOM_INFO_QUIET, 6,
									 "Error help: This is an abnormal situation, as the mixer special device was "
									 "opened correctly, but I can't read it. "
									 GOM_DRIVER_BUG_DONNO);
		return 0;
	}
	else if (channelmask == 0) /* Test: are there channels on mixer ? */
	{
		close(new_mixer);
		gom_info(GOM_INFO_ERROR,
						 "Bug: Mixer opened ok, but no channels available (!?) (opened special file is %s).",
						 device_file);
		gom_info_block(GOM_INFO_QUIET, 6,
									 "Error help: This is an abnormal situation, as your mixer presents himself to "
									 "me as being available with _zero_ channels. "
									 GOM_DRIVER_BUG_DONNO);
		return 0;
	}
	else /* everything ok to open the new mixer */
	{
		/* close the old mixer device */
		gom_driver_d_set(std_verb, NULL);

		/* set the new values */
		strncpy(gom_driver_oss.c.fn, device_file, GOM_DEVICE_FILE_SIZE);
		gom_driver_oss.c.fd = new_mixer;
		gom_driver_oss.c.opened = 1;
		gom_info(std_verb,
						 "Mixer corresponding to special file %s opened.",
						 device_file);

		/* read the mixer life time constants */
		ioctl(gom_driver_oss.c.fd, MIXER_READ(SOUND_MIXER_DEVMASK),    &gom_driver_oss.c.channelmask);
		ioctl(gom_driver_oss.c.fd, MIXER_READ(SOUND_MIXER_RECMASK),    &gom_driver_oss.c.recmask);
		ioctl(gom_driver_oss.c.fd, MIXER_READ(SOUND_MIXER_STEREODEVS), &gom_driver_oss.c.stereomask);

		/* Create starting configuration */
		gom_driver_oss.v.lockmask = ~0;

		/* we seem to have had success */
		return 1;
	}
}

/*
 * MIXER CHANNELS KNOWN TO THIS DRIVER
 */

int gom_driver_C (int c)
{
	if ((c >= 0) && (c < GOM_DRIVER_CHANNELS))
		return c;
	else
		return -1;
}

int gom_driver_C_first ()
{
	return 0;
}

int gom_driver_C_last ()
{
	return GOM_DRIVER_CHANNELS-1;
}

int gom_driver_C_next(int c, int jump)
{
	return gom_driver_C (c+jump);
}

int gom_driver_C_len ()
{
	return GOM_DRIVER_CHANNELS;
}

char * gom_driver_C_name (unsigned c, int label)
{
	char * channel_name[GOM_DRIVER_CHANNELS] = SOUND_DEVICE_NAMES;
	char * channel_label[GOM_DRIVER_CHANNELS] = SOUND_DEVICE_LABELS;
	static char unexistent[] = "no_OSS_channel";

	if (gom_driver_C (c) > -1)
	{
		if (label)
			return channel_label[c];
		else
			return channel_name[c];
	}
	else
		return unexistent;
}

/*
 * AVAILABLE CHANNELS
 */

int gom_driver_c (int c)
{
	if (gom_driver_oss.c.opened && (gom_driver_private_getbits (gom_driver_oss.c.channelmask, c, 1)))
		return gom_driver_C (c);
	else
		return -1;
}

int gom_driver_c_first ()
{
	int c;
	for (c = gom_driver_C_first ();
			 (c > -1) && (gom_driver_c (c) < 0);
			 c = gom_driver_C_next (c, +1));
	return c;
}

int gom_driver_c_last ()
{
	int c;
	for (c = gom_driver_C_last ();
			 (c > -1) && (gom_driver_c (c) < 0);
			 c = gom_driver_C_next (c, -1));
	return c;
}

int gom_driver_c_next (int c, int jump)
{
	do
		c = gom_driver_C_next(c, jump);
	while ((gom_driver_C (c) > -1) && (gom_driver_c (c) < 0));
	return c;
}

int gom_driver_c_len ()
{
	int i, avail_channels = 0;

	for (i = gom_driver_c_first();
			 i != -1;
			 i = gom_driver_c_next(i, +1))
		avail_channels++;
	return avail_channels;
}

char * gom_driver_c_name (int c, int label)
{
	static char unavailable[] = "unavailable";

	if (gom_driver_c (c) < 0)
		return unavailable;
	else
		return gom_driver_C_name (c, label);
}

/*
 * CHANNEL CHANNELS
 */

int gom_driver_C_C (int c, int cc)
{
	if ((gom_driver_C (c) > -1)
			&& (cc >= gom_driver_C_C_first (c)) && (cc <= gom_driver_C_C_last (c)))
		return cc;
	else
		return -1;
}

int gom_driver_C_C_first (int c)
{
	return 0;
}

int gom_driver_C_C_last (int c)
{
	return gom_driver_C_C_len (c)-1;
}

int gom_driver_C_C_next(int c, int cc, int jump)
{
	return gom_driver_C_C (c, cc+jump);
}

int gom_driver_C_C_len (int c)
{
	if (gom_driver_C (c) > -1)
		/* OSS only supports max. 2 channels */
		return GOM_DRIVER_CHANNEL_CHANNELS;
	else
		return -1;
}

/*
 * AVAILABLE CHANNEL CHANNELS
 */

int gom_driver_c_c (int c, int cc)
{
	if ((gom_driver_c (c) > -1)
			&& (cc >= gom_driver_c_c_first (c)) && (cc <= gom_driver_c_c_last (c)))
		return cc;
	else
		return -1;
}

int gom_driver_c_c_first (int c)
{
	return 0;
}

int gom_driver_c_c_last (int c)
{
	return gom_driver_c_c_len (c)-1;
}

int gom_driver_c_c_next(int c, int cc, int jump)
{
	return gom_driver_c_c (c, cc+jump);
}

int gom_driver_c_c_len (int c)
{
	if (gom_driver_c (c) > -1)
		/* OSS only supports max. 2 channels */
		return 1+gom_driver_private_getbits(gom_driver_oss.c.stereomask, c, 1);
	else
		return -1;
}

/*
 * VOLUMES
 */
int gom_driver_c_c_v_max()
{
	return 100;
}

int gom_driver_c_c_v_range(int v)
{
	if ((v > -1) && (v <= gom_driver_c_c_v_max()))
		return v;
	else
	{
		gom_info(GOM_INFO_ERROR,
						 "Requested volume (%d) out of range (allowed range: 0 =< volume <= %d).",
						 v, gom_driver_c_c_v_max());
		return -1;
	}
}

int gom_driver_c_c_v (int c, int cc)
{
	int volume=0;

	if (gom_driver_c_c(c, cc) > -1)
	{
		if (ioctl(gom_driver_oss.c.fd, MIXER_READ(c), &volume) != -1)
		{
			return gom_driver_private_getbits(volume, (8*(cc+1))-1, 8);
		}
		else
		{
			gom_info(GOM_INFO_ERROR,
							 "Bug (gom_driver_c_c_v): Can't read volume on %d(%s).",
							 c, gom_driver_C_name(c, 0));
		}
	}
	return -1;
}


/* Setting volumes: standard */
void gom_driver_c_c_v_set(enum gom_info_types std_verb,
										 int c, int cc, int v)
{
	int coded_volume;
	int old_v = gom_driver_c_c_v (c, cc);
	v = gom_driver_c_c_v_range (v);

	if (old_v < 0)
	{
		gom_info(GOM_INFO_ERROR,
						 "Can't set volume %d on channel %d(%s): not available.",
						 cc,
						 c, gom_driver_C_name(c, 0));
	}
	else if (v < 0)
	{
		/* error already shown */
	}
	else
	{
		old_v = gom_driver_c_c_v(c, cc);

		/* in OSS, you set all volumes once, so we have to code all values into one */
		/* OSS only supports two volumes (left/right) */
		if (cc == 0)
			/* setting left volume, encoding old right volume */
			coded_volume = v | gom_driver_c_c_v(c, 1) << 8;
		else if (cc == 1)
			/* setting right volume, encoding old left volume */
			coded_volume = gom_driver_c_c_v(c, 0) | v << 8;
		else
			gom_info(GOM_INFO_ERROR,
							 "Bug (gom_driver_c_c_v_set): cc too big.");

		if (ioctl(gom_driver_oss.c.fd, MIXER_WRITE(c), &coded_volume) != -1)
		{
			gom_info(std_verb,
							 "Volume %d on channel %d(%s) set to %i (from %i).",
							 cc,
							 c, gom_driver_C_name(c, 0),
							 gom_driver_c_c_v(c, cc), old_v);
		}
		else
		{
			gom_info(GOM_INFO_ERROR,
							 "Bug (gom_driver_c_c_v_set): Can't write volume to mixer device though available (channel %d(%s)).",
							 c, gom_driver_C_name(c, 0));
		}
	}
}

/*
 * RECORD
 */

int gom_driver_c_r (int c)
{
	unsigned recsrc;
	if ((gom_driver_c (c) > -1) && gom_driver_private_getbits(gom_driver_oss.c.recmask, c, 1))
	{
		if (ioctl(gom_driver_oss.c.fd, MIXER_READ(SOUND_MIXER_RECSRC), &recsrc) == -1)
			gom_info(GOM_INFO_ERROR, "Bug (gom_driver_c_r): Can't read recording source mask from mixer.");
		return gom_driver_private_getbits(recsrc, c, 1);
	}
	else
		return -1;
}

void gom_driver_c_r_set(enum gom_info_types std_verb,
									 int c, int r)
{
	unsigned recsrc;
	int old_r = gom_driver_c_r (c);

	if (old_r > -1)
	{
		ioctl(gom_driver_oss.c.fd, MIXER_READ(SOUND_MIXER_RECSRC), &recsrc);
		recsrc = gom_driver_private_setbit(recsrc, c, r);
		/* rec-src == 0 would lead to nasty default mic!! */
		if (recsrc != 0)
			ioctl(gom_driver_oss.c.fd, MIXER_WRITE(SOUND_MIXER_RECSRC), &recsrc);

		if (gom_driver_c_r(c) == r)
		{
			gom_info(std_verb,
							 "Recording %s for channel %d(%s).",
							 r ? "on" : "off",
							 c, gom_driver_C_name(c, 0));
		}
		else
		{
			gom_info(GOM_INFO_ERROR,
							 "Can't set recording for channel %d(%s) to %s: %s.",
							 c, gom_driver_C_name(c, 0),
							 r ? "on" : "off",
							 "maybe last recording source");
		}
	}
	else
	{
		gom_info(GOM_INFO_ERROR,
						 "Can't set recording for channel %d(%s): %s.",
						 c, gom_driver_C_name(c, 0),
						 (gom_driver_c (c) < 0) ? "unavailable" : (gom_driver_c_r (c) < 0)? "unrecordable" : "*bug*");
	}
}
