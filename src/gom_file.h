/* gom, Gom is nOt yet another (Audio) Mixer.
 *
 * Copyright (C) 1996-2004  Stephan Sürken <absurd@olurdix.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * gom_file.h: loading and saving files
 */

/*
 * INCLUDES
 */

/*
 * MACROS
 */

/*
 * DECLARATIONS/DEFINTIONS
 */

/* These used to be sucking global ;) */

/* char gom_file_fn[FILENAME_MAX]; */
/* FILE * gom_file_f; */

/*
 * FUNCTION PROTOTYPES
 */

/*
 * LOADING
 */

/* loading options from stream: */
void gom_file_options_read(enum gom_info_types std_verb, FILE * f, char * allowed_actions);

/* loading any options file */
void gom_file_load(enum gom_info_types verb, char * fname, int is_settings_file, char * allowed_actions);

/* void  */
/* gom_file_options_load(enum gom_info_types std_verb, */
/* 		      char * fname, */
/* 		      enum gom_info_types open_err_verb, int is_settings_file, char * allowed_actions, */
/* 		      int force_sysconf); */

/* void */
/* gom_file_config_load(enum gom_info_types verb, char * fname, char * allowed_actions); */

/*
 * SAVING
 */

/* settings file to stream */
void gom_file_settings_write(FILE * f, int verbose);

/* settings file to file: */
void gom_file_settings_save(enum gom_info_types std_verb, char * fname);
