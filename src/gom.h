/* gom, Gom is nOt yet another (Audio) Mixer.
 *
 * Copyright (C) 1996-2004  Stephan Sürken <absurd@olurdix.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */
/*
 * gom.h GOM Header file
 */

#ifndef GOM_H
#define GOM_H

/*
 * INCLUDES
 */

/* Project Configuration. */
#include "config.h"

/* ANSI C headers */
#include <stdio.h>
#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <signal.h>
#include <limits.h>

/* System headers */
#include <sys/time.h>
#include <sys/ioctl.h>
/* "The nice thing about standards..." */
#if defined (__linux__) || defined (__bsdi__) || defined (HAVE_SYS_SOUNDCARD_H)
#include <sys/soundcard.h>
#else
#if defined (__FreeBSD__)
#include <machine/soundcard.h>
#else
#if defined (__NetBSD__) || defined (__OpenBSD__)
#include <soundcard.h> /* OSS emulation--need to link to ossaudio library*/
#endif
#endif
#endif
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>

/* GNU headers */
#if defined(HAVE_GETOPT_H)
#include <getopt.h>
#endif /* HAVE_GETOPT_H */

/* GOM headers */
#include "gom_info.h"
#include "gom_driver.h"
#include "gom_mixer.h"
#include "gom_gomii.h"
#include "gom_file.h"
#include "gom_action.h"
#include "gom_iface.h"

/*
 * MACROS
 */
/* check for makefile-side defines */
#if !defined(GOM_VERSION)
#define GOM_VERSION "no version: GOM_VERSION not defined when building gom."
#endif
#if !defined(GOM_DATE)
#define GOM_DATE "no date: GOM_DATE not defined when building gom."
#endif

/* version string: is makefile-dependent */
#define GOM_VERSION_STRING "gom " GOM_VERSION " (" GOM_DATE ")"

/* some basic configuration (there should be no need to change these) */
#define GOM_DEVICE_FILE_DEFAULT     "/dev/mixer"
#define GOM_DEVICE_FILE_SIZE        80
#define GOM_DEVICE_FILE_SIZE_AS_STR "80"
#define GOM_DEVICE_NOT_OPENED_TEXT  "no_mixer_opened"

/* File names for _option_ files: roughly, this sums up to: */
/*  gom/conf.gom               (restricted to -F, -U) */
/*  gom/conf.default_mixer     (restricted to -d)     */
/*  gom/conf.initialize        (unrestricted)         */
/*  gom/<mixer-device>.<name>  (for 'auto'-saved|loaded settings) */

#define GOM_CONFIG_FILE_PREFIX         "conf"
#define GOM_CONFIG_DIR_USER            ".gom/"
#define GOM_CONFIG_DIR_SYSTEM          "/etc/gom/"

#define GOM_CONFIG_FILE                "gom"
#define GOM_CONFIG_FILE_ALLOWED        "vqFU"

#define GOM_DEFAULT_MIXER_FILE         "default_mixer"
#define GOM_DEFAULT_MIXER_FILE_ALLOWED "d"

#define GOM_INIT_FILE                  "initialize"

#define GOM_OPTIONS_FILE_DEFAULT       "default"

/* fade interval */
#define GOM_MIXER_FADIVAL_MIN               1   /* seconds */
#define GOM_MIXER_FADIVAL_MAX            1800   /* 1800 seconds = 30 mins */
#define GOM_MIXER_FADIVAL_DEFAULT           5   /* seconds */
#define GOM_MIXER_FADIVAL_DEFAULT_QUOTED   "5"  /* seconds */

/* refresh interval */
#define GOM_GOMII_REFIVAL_MIN                0  /* 0 disables */
#define GOM_GOMII_REFIVAL_MAX             3600  /* 3600 seconds = 1 h */
#define GOM_GOMII_REFIVAL_DEFAULT           30  /* seconds */
#define GOM_GOMII_REFIVAL_DEFAULT_QUOTED   "30" /* seconds */

/*
 * DECLARATIONS/DEFINTIONS
 */

/*
 * FUNCTION PROTOTYPES
 */

/*
 * GOM STANDARD INCLUDES
 */

/*
 * OVERALL MACROS
 */

#endif /* gom_h */
