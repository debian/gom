/* gom, Gom is nOt yet another (Audio) Mixer.
 *
 * Copyright (C) 1996-2004  Stephan Sürken <absurd@olurdix.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * gom_gomii: gomii API
 */

/*
 * INCLUDES
 */

/*
 * MACROS
 */

/*
 * DECLARATIONS/DEFINTIONS
 */

/* for gomii refresh */
enum gom_gomii_refresh_stage {GOM_GOMII_REFRESH_NO,
															GOM_GOMII_REFRESH_INIT,
															GOM_GOMII_REFRESH_PAGE,
															GOM_GOMII_REFRESH_CHANNELS,
															GOM_GOMII_REFRESH_CURRENT_CHANNEL};

/*
 * FUNCTION PROTOTYPES
 */

typedef void (*RefProcFunc)();

/*
 * GOMII REFRESH INTERVAL
 */
void gom_gomii_refproc_keep();
int gom_gomii_refival();

int gom_gomii_refival_set(enum gom_info_types std_verb, int interval, RefProcFunc refresh_proc);
char * gom_gomii_last_refresh_str();
