.\"
.\" @GOM_COPYRIGHT@
.\"
.TH GOMCONFIG 8 "@DATE@" "3rd party" "Contributed tool"

.SH NAME
gomconfig \- configuration tool for gom, a generic audio mixer

.SH SYNOPSIS
.BI "gomconfig [--version | --force | --purge]"

.SH VERSION
.BR "" "This manual page was distributed with " "@PACKAGE@ @VERSION@" " (@DATE@)."

.SH DESCRIPTION
.LP
.BR "gomconfig" " is an interactive, text based configuration tool for
gom. It deals with files in ~/.gom/ for a normal user, and with files
in /etc/gom/ for the superuser.

If gomconfig is run without arguments, it will check the validity of
the current configuration and only go interactive if it is invalid.

Please see
.IR "gom" "(1)"
for details about gom's configuration.

.SH OPTIONS

.TP
.B --version
Display version information.
.TP
.B --force
Forces reconfiguration (even if current configuration is valid).
.TP
.B --purge
Removes configuration directory (~/.gom or /etc/gom, respectively)
completely. This will run non-interactively, but will create a
temporary backup.

.SH AUTHOR

.BR "Gomconfig" " is part of the gom package."

@GOM_COPYRIGHT@

See 
.IR "gom" "(1)"
for details.

.SH BUGS

Gomconfig is probably overkill; you might prefer to simply use an
editor (and gom itself) to change the configuration.

.SH SEE ALSO

.IR "gom" "(1)"
