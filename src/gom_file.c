/* gom, Gom is nOt yet another (Audio) Mixer.
 *
 * Copyright (C) 1996-2004  Stephan Sürken <absurd@olurdix.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * gom_file.c: loading and saving files
 */

/*
 * INCLUDES
 */
#include "gom.h"

/*
 * MACROS
 */

#define GOM_FILE_OPT_SIZE         100
#define GOM_FILE_OPT_SIZE_QUOTED "100"

/*
 * DECLARATIONS/DEFINTIONS
 */

char gom_file_option[GOM_FILE_OPT_SIZE];

/*
 * FUNCTION PROTOTYPES
 */

/*****************/

/*
 * INTERN HELPERS
 */

/* test for root privileges */
int gom_file_systemconf()
{
	return (geteuid() == 0) || (getuid() == 0);
}

char * gom_file_fncat(char fn[FILENAME_MAX], const char * ct)
{
	if (strlen(ct) > FILENAME_MAX-strlen(fn)-1)
		gom_info(GOM_INFO_ERROR, "Bug: File name too big (>%i characters).", FILENAME_MAX-1);
	return strncat(fn, ct, FILENAME_MAX-strlen(fn)-1);
}

int gom_file_fn_make(enum gom_info_types verb,
										 char fn[FILENAME_MAX],
										 const char * fname,
										 const int is_settings_file,
										 const int force_systemconf)
{
	char * mixer_name;

	fn[0] = '\0';
	if (fname != NULL && (fname[0] == '/' || fname[0] == '~' || (fname[0] == '.' && fname[1] == '/')))
	{
		/* absolute or explicit filename (/<path> or ./<path>):
			 - take it as it is
			 - no overwrite */
		if (fname[0] == '~' && getenv("HOME") != NULL)
		{
			gom_file_fncat(fn, getenv("HOME"));
			gom_file_fncat(fn, fname+1);
		}
		else
		{
			gom_file_fncat(fn, fname);
		};
		return 0;  /* no overwrite */
	}
	else
	{
		/* simple filename; overwrite
			 - generate filename (~/.gom/<mixer>.<fname>)
			 - overwrite */
		if (force_systemconf || gom_file_systemconf())
		{
			/* superuser privileges: write to system directory */
			gom_file_fncat(fn, GOM_CONFIG_DIR_SYSTEM);
		}
		else
			if (getenv("HOME") != NULL)
			{
				/* normal user; write to ~/.gom */
				gom_file_fncat(fn, getenv("HOME"));
				gom_file_fncat(fn, "/" GOM_CONFIG_DIR_USER);
			}
			else
			{
				/* warning: normal user, but no HOME env */
				gom_info(GOM_INFO_QUIET, "Warning: No environment variable 'HOME' found (using working directory).");
				gom_file_fncat(fn, "./" GOM_CONFIG_DIR_USER);
			}

		if (is_settings_file)
		{
			/* create '<device>.filename' */
			/* get '<device>' from '/dev/<device>' */
			mixer_name = rindex(gom_driver_d(), '/');
			if (mixer_name == NULL)
			{
				/* no mixer opened (or some other error?) */
				mixer_name = gom_driver_d();
			}
			else
			{
				mixer_name++;
			}
			gom_file_fncat(fn, mixer_name);
			gom_file_fncat(fn, ".");
		}
		else
			/* create 'conf.<filename>' */
		{
			gom_file_fncat(fn, GOM_CONFIG_FILE_PREFIX);
			gom_file_fncat(fn, ".");
		}

		gom_file_fncat(fn, (fname == NULL) || (strlen(fname) == 0) ? GOM_OPTIONS_FILE_DEFAULT : fname);
		return 1;  /* ok to overwrite */
	}
}


/*
 * LOADING
 */


/* intern */
char * gom_file_optarg_get()
{
	static char nullstr[] = "";

	if (strlen(gom_file_option) < 2)
		/* gom_file_option == "d" or worse */
		return nullstr;
	else
		/* seems to be valid */
		return (char *) &gom_file_option[1];
}


char gom_file_fprereadc(FILE * f)
{
	char c;
	if (1 == fscanf(f, "%c", &c))
		fseek(f, -1L, SEEK_CUR);
	else
		c = EOF;
	return c;
}

/* Check if actions is allowed; (allowed_actions == NULL) ==> all options are ok */
int gom_file_option_allowed(char * allowed_actions, char action)
{
	if (allowed_actions == NULL)
		return 1;
	else
		return (index(allowed_actions, action) != NULL);
}

/* loading options from stream: */
void gom_file_options_read(enum gom_info_types std_verb,
													 FILE * f, char * allowed_actions)
{
	char ind, action;
	int count, line = 1, line_valid;

	do
	{
		/* valid lines MUST start with a dash; pre-read first char of line to check */
		line_valid = ((ind = gom_file_fprereadc(f)) == '-');

		if (line_valid)
		{
			gom_info(std_verb+2, "Line %d: significant, processing...", line);

			while (1 == (count = fscanf(f,
																	"-%" GOM_FILE_OPT_SIZE_QUOTED "s",
																	(char *) &gom_file_option)))
			{
				/* validity check? */

				action = gom_file_option[0];

				gom_info(std_verb+2, "Line %d: Option found: %s", line, gom_file_option);

				if (gom_file_option_allowed(allowed_actions, action))
					gom_action(std_verb, action, 1, gom_file_optarg_get, NULL, NULL);
				else
					gom_info(GOM_INFO_ERROR,
									 "Option \"-%s\" is not allowed here (allowed are: \"%s\").",
									 &gom_file_option, allowed_actions);

				/* a new following space indicates more options */
				if (isspace(ind = gom_file_fprereadc(f)) && (ind != '\n'))
					/* trennzeichen, aber kein newline */
					fseek(f, +1L, SEEK_CUR);
				else
					/* kein trennzeichen, evt. newline */
					break;
			}
		}
		else
		{
			/* this is only for debugging config files... */
			if (ind == '#')
				gom_info(std_verb+2, "Line %d: comment, skipping...", line);
			else if (ind == '\n')
				gom_info(std_verb+2, "Line %d: empty, skipping...", line);
			else
				gom_info(std_verb+2, "Line %d: ambigious, skipping...", line);
		}

		/* skipping to the end of this line */
		while ((1 == fscanf(f, "%c", &ind)) && (ind != EOF) && (ind != '\n'))
		{
			if (line_valid)
				gom_info(std_verb+2, "Line %d: Skipping suspicious trailing garbage (%c).", line, ind);
		};

		line++;
	}
	while (feof(f) == 0);
}

/* loading any gom option file */
/* - 1st tries user, 2nd system file */
/* - 'is_settings_file' needed to distinguish conf.* / mixer.* */

void gom_file_load(enum gom_info_types verb, char * fname,
                   int is_settings_file, char * allowed_actions)
{
	FILE * f;
	char fn_user[FILENAME_MAX];
	char fn_sys[FILENAME_MAX];
	char * fn = NULL;
	enum gom_info_types open_err_verb = is_settings_file ? GOM_INFO_ERROR : verb;

	/* Try to open user file: ~/.gom/file */
	gom_file_fn_make (verb, fn_user, fname, is_settings_file, 0);
	if ((f = fopen(fn_user, "r")) == NULL)
	{
		/* Try to open system file: /etc/gom/file */
		gom_file_fn_make (verb, fn_sys, fname, is_settings_file, 1);
		if ((f = fopen(fn_sys, "r")) == NULL)
		{
			gom_info(open_err_verb,
							 "No such file from stem \"%s\" (tried \"%s\" and \"%s\").",
							 fname, fn_user, fn_sys);
			return;
		}
		else
		{
			fn = fn_sys;
		}
	}
	else
	{
		fn = fn_user;
	}

	/* at this point, f must be open */
	gom_info(verb, "Loading option file from \"%s\"...", fn);
	gom_file_options_read(verb+1, f, allowed_actions);
	gom_info(verb, "Option file load from \"%s\" completed.", fn);

	/* Do I need to check if it was really closed? */
	fclose(f);
}

/*
 * SAVING
 */

/* intern helps to stream */
int gom_file_stropt_write(FILE * f, char option, char * arg)
{
	return fprintf(f, "-%c%s ", option, arg);
}

int gom_file_intopt_write(FILE * f, char option, int arg)
{
	return fprintf(f, "-%c%i ", option, arg);
}

/* write ONE CHANNEL */
void gom_file_options_c_write(FILE * f, int c)
{
	int cc;

	/* select channel, write record */
	gom_file_intopt_write(f, 'c', c);
	if (gom_driver_c_r(c) > -1)
		gom_file_intopt_write(f, 'r', gom_driver_c_r(c));

	/* unlock channel to get the different volumes right when *loading* */
	gom_file_intopt_write(f, 'k', 0);

	/* write all channel volumes */
	for (cc=gom_driver_c_c_first(c); cc > -1; cc=gom_driver_c_c_next(c, cc, 1))
	{
		gom_file_intopt_write(f, 'C', cc);
		gom_file_intopt_write(f, 'l', gom_driver_c_c_v(c, cc));
	}
	/* _lock_ channel as bulls neck default when *loading* */
	gom_file_intopt_write(f, 'k', 1);
}

/* settings file to stream */
void gom_file_settings_write(FILE * f, int verbose)
{
	int c;

	if (verbose)
	{
		fprintf(f,
						"# This is a mixer settings file, automatically produced by " GOM_VERSION_STRING ".\n"
						"# Lines starting with \"-\" are read (must be one-letter style gom options).\n"
						"# Other lines are comments.\n"
						"#\n"
						"# Channels with recording source == 1 first (avoids \"last rec. source error\" when loading):\n");
	}

	for (c=gom_driver_c_first(); c > -1; c=gom_driver_c_next(c, 1))
	{
		if (gom_driver_c_r(c) > 0)
		{
			gom_file_options_c_write(f, c);
			if (verbose)
				fprintf(f, "\n");
		}
	}

	if (verbose)
		fprintf(f,
						"# Other channels:\n");
	for (c=gom_driver_c_first(); c > -1; c=gom_driver_c_next(c, 1))
	{
		if (gom_driver_c_r(c) <= 0)
		{
			gom_file_options_c_write(f, c);
			if (verbose)
				fprintf(f, "\n");
		}
	}
	if (verbose)
		fprintf(f, "# Resetting current channel to first.\n");
	gom_file_intopt_write(f, 'c', gom_driver_c_first());
}

/* settings file to file: */
void gom_file_settings_save(enum gom_info_types std_verb, char * fname)
{
	char fn[FILENAME_MAX];
	FILE * f = NULL;

	int overwrite;

	overwrite = gom_file_fn_make(std_verb, fn, fname, 1, 0);

	/* NEVER write the config file as setting */
	if ((fname != NULL) && (strncmp(fname, GOM_CONFIG_FILE, strlen(GOM_CONFIG_FILE)) == 0))
	{
		gom_info(GOM_INFO_ERROR,
						 "I won't save files starting with \"" GOM_CONFIG_FILE "\" (like \"%s\") to the configuration dir.",
						 fn);
	}
	/* try open file */
	else if (!overwrite && (f = fopen(fn, "r")) != NULL)  /* open absolute given file */
	{
		fclose(f); f = NULL;
		gom_info(GOM_INFO_ERROR,
						 "File \"%s\" exists (I refuse to overwrite files elsewhere than in $HOME/.gom).",
						 fn);
	}
	else if ((f = fopen(fn, "w")) == NULL)  /* open relative given file */
	{
		gom_info(GOM_INFO_ERROR,
						 "Can't open file \"%s\" for save: %s.",
						 fn, strerror(errno));
	}
	else
	{
		/* ok to overwrite */
		if (gom_file_systemconf())
			gom_info(GOM_INFO_QUIET, "Warning: Running as root, writing to \"" GOM_CONFIG_DIR_SYSTEM "\".");

		/* write a comment header */
		gom_file_settings_write(f, 1);

		if (fclose(f) == 0)
			gom_info(std_verb, "Settings saved to \"%s\".", fn);
		else
			gom_info(GOM_INFO_ERROR, "Problems closing file \"%s\": %s.",
							 fn, strerror(errno));
	}
}
