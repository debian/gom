/* gom, Gom is nOt yet another (Audio) Mixer.
 *
 * Copyright (C) 1996-2004  Stephan Sürken <absurd@olurdix.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * gom_mixer.c: gom-special mixer functions
 */

/*
 * INCLUDES
 */
#include "gom.h"

/*
 * MACROS
 */

/*
 * DECLARATIONS/DEFINTIONS
 */

int gom_mixer_c_current_var;
int gom_mixer_c_c_current_var;
int gom_mixer_fadival_var;  /* seconds */

Gom_mixer_settings gom_mixer_snapshot_var;

int gom_mixer_unlockmask[GOM_DRIVER_CHANNELS] = {0};

/*
 * FUNCTION PROTOTYPES
 */

/*****************/

/*
 * MIXER
 */
void gom_mixer_d_defaults_set(enum gom_info_types std_verb)
{
	/* all locks on */
	gom_mixer_l_set(std_verb, 1);

	/* current channel, current volume channel */
	gom_mixer_c_current_set(std_verb, gom_driver_c_first());
	gom_mixer_c_c_current_set(std_verb, gom_driver_c_c_first(gom_mixer_c_current()));

	/* make new default snapshot */
	gom_mixer_snapshot(std_verb);
}

/*
 * FIND CHANNEL FROM NAME
 */
int gom_mixer_C_find(char * name)
{
	int c;
	for (c = gom_driver_C_first();
			 (gom_driver_C (c) > -1) && (strcmp (name, gom_driver_C_name(c, 0)) != 0);
			 c = gom_driver_C_next(c, +1));
	return c;
}

/*
 * CURRENT CHANNEL/VOLUME: just holds channel/volume + integrity checks
 */

int gom_mixer_c_current()
{
	return gom_mixer_c_current_var;
}

int gom_mixer_c_c_current()
{
	return gom_mixer_c_c_current_var;
}

void gom_mixer_c_current_set(enum gom_info_types std_verb, int c)
{
	if (gom_driver_c (c) > -1)
	{
		gom_mixer_c_current_var = c;
		gom_info(std_verb,
						 "Setting current channel to %d(%s).",
						 c, gom_driver_c_name(c, 0));
	}
	else
	{
		gom_info(GOM_INFO_ERROR,
						 "Can't make channel %d=(%s) current: unavailable.",
						 c,
						 gom_driver_C_name(c, 0));
	}
}

void gom_mixer_c_c_current_set(enum gom_info_types std_verb, int cc)
{
	int c = gom_mixer_c_current();

	if (gom_driver_c_c (c, cc) > -1)
	{
		gom_mixer_c_c_current_var = cc;
		gom_info(std_verb,
						 "Setting current volume to %d on channel %d(%s).",
						 cc,
						 c, gom_driver_c_name(c, 0));
	}
	else
	{
		gom_info(GOM_INFO_ERROR,
						 "Can't set volume to %d on channel %d(%s): volume unavailable.",
						 cc,
						 c, gom_driver_C_name(c, 0));
	}
}

/*
 * MUTE
 */
void gom_mixer_c_mute(enum gom_info_types std_verb, int c)
{
	if (gom_driver_c (c) > -1)
	{
		gom_mixer_c_v_set(std_verb+1, c, 0);
		gom_info(std_verb,
						 "Channel %d(%s) muted.",
						 c, gom_driver_c_name(c, 0));
	}
	else
	{
		gom_info(GOM_INFO_ERROR,
						 "Can't mute channel %d(%s): unavailable.",
						 c, gom_driver_C_name(c, 0));
	}
}

void gom_mixer_mute(enum gom_info_types std_verb)
{
	int c;

	for (c=gom_driver_c_first(); c > -1; c=gom_driver_c_next(c, 1))
		gom_mixer_c_mute(std_verb+1, c);

	gom_info(std_verb, "All channels muted.");
}


/*
 * FADE
 */
int gom_mixer_fadival()
{
	return gom_mixer_fadival_var;
}

int gom_mixer_fadival_set(enum gom_info_types std_verb, int duration)
{
	int oldduration = gom_mixer_fadival();
	if ((duration >= GOM_MIXER_FADIVAL_MIN)
			&& (duration <= GOM_MIXER_FADIVAL_MAX))
	{
		gom_mixer_fadival_var = duration;
		gom_info(std_verb,
						 "Fade duration set to %d seconds (was %d).", duration, oldduration);
	}
	else
		gom_info(GOM_INFO_ERROR,
						 "Requested fade duration (%d seconds) out of range (allowed range: %d < duration <= %d).",
						 duration, GOM_MIXER_FADIVAL_MIN, GOM_MIXER_FADIVAL_MAX);

	return oldduration;
}

void gom_mixer_c_c_v_fade_meta(enum gom_info_types std_verb, int c, int cc, int v, int locksensible)
{
	int volume, diff;
	unsigned long micro_seconds_to_sleep = 5000;
	int fade_up;

	int old_v = gom_driver_c_c_v (c, cc);
	v = gom_driver_c_c_v_range (v);

	if (old_v < 0)
	{
		gom_info(GOM_INFO_ERROR,
						 "Can't fade volume %d on channel %d(%s): not available.",
						 cc, c, gom_driver_C_name(c, 0));
	}
	else if (v < 0)
	{
		/* error already shown */
	}
	else
	{
		/* HAIRY: get the message right... */
		if (locksensible && gom_mixer_c_l(c))
			gom_info(std_verb,
							 "Fading all volumes of channel %d(%s) from %i to %i in about %i seconds.",
							 c, gom_driver_c_name(c, 0),
							 gom_driver_c_c_v(c, cc), v, gom_mixer_fadival());
		else
			gom_info(std_verb,
							 "Fading volume %d of channel %d(%s) from %i to %i in about %i seconds.",
							 cc,
							 c, gom_driver_c_name(c, 0), gom_driver_c_c_v(c, cc), v, gom_mixer_fadival());

		/* actually fade */
		fade_up = (gom_driver_c_c_v(c, cc) <= v);
		diff = (fade_up ? (v - gom_driver_c_c_v(c, cc)) : (gom_driver_c_c_v(c, cc)-v));
		micro_seconds_to_sleep = (1000000 * gom_mixer_fadival());

		micro_seconds_to_sleep = micro_seconds_to_sleep / (diff != 0 ? diff : 1);

		for (fade_up ? (volume = gom_driver_c_c_v(c, cc)+1) : (volume = gom_driver_c_c_v(c, cc)-1);
				 fade_up ? volume < v        : volume > v;
				 fade_up ? ++volume          : --volume)
		{
			if (locksensible)
				gom_mixer_c_c_v_set_l(std_verb+1, c, cc, volume);
			else
				gom_driver_c_c_v_set(std_verb+1, c, cc, volume);
			usleep(micro_seconds_to_sleep);
		}
		if (locksensible)
			gom_mixer_c_c_v_set_l(std_verb+1, c, cc, v);
		else
			gom_driver_c_c_v_set(std_verb+1, c, cc, v);
	}
}

void gom_mixer_c_c_v_fade(enum gom_info_types std_verb, int c, int cc, int v)
{
	gom_mixer_c_c_v_fade_meta(std_verb, c, cc, v, 0);
}

/* Fading volume: lock-sensible */
void gom_mixer_c_c_v_fade_l(enum gom_info_types std_verb, int c, int cc, int v)
{
	gom_mixer_c_c_v_fade_meta(std_verb, c, cc, v, 1);
}

/* Znapshot settings */
void gom_mixer_snapshot(enum gom_info_types std_verb)
{
	int c, cc;

	/* we go through all mixer channels; -1 for unavailable */
	for (c=gom_driver_C_first(); c > -1; c=gom_driver_C_next(c, 1))
	{
		gom_mixer_snapshot_var.record[c] = gom_driver_c_r(c);
		for (cc=gom_driver_C_C_first(c); cc > -1; cc=gom_driver_C_C_next(c, cc, 1))
			gom_mixer_snapshot_var.volume[c][cc] = gom_driver_c_c_v(c, cc);
	}
	gom_info(std_verb, "Snapshot of current settings made.");
}

/* help: unsnapshot ONE channel */
void gom_mixer_unsnapshot_c(enum gom_info_types std_verb, int c)
{
	int cc;

	if (gom_mixer_snapshot_var.record[c] > -1)
		gom_driver_c_r_set(std_verb+1, c, gom_mixer_snapshot_var.record[c]);
	for (cc=gom_driver_c_c_first(c); cc > -1; cc=gom_driver_c_c_next(c, cc, 1))
		gom_driver_c_c_v_set(std_verb+1, c, cc, gom_mixer_snapshot_var.volume[c][cc]);

	gom_info(std_verb, "Snapshot settings for channel %i restored.", c);
}

void gom_mixer_unsnapshot(enum gom_info_types std_verb)
{
	int c;

	/* the snapshot must have been made on the same mixer */

	/* first, unsnap all all channels with rec src on (void "last rec src err") */
	for (c=gom_driver_c_first(); c > -1; c=gom_driver_c_next(c, 1))
	{
		if (gom_mixer_snapshot_var.record[c] > 0)
			gom_mixer_unsnapshot_c(std_verb+1, c);
	}

	/* now the others */
	for (c=gom_driver_c_first(); c > -1; c=gom_driver_c_next(c, 1))
	{
		if (gom_mixer_snapshot_var.record[c] <= 0)
			gom_mixer_unsnapshot_c(std_verb+1, c);
	}

	gom_info(std_verb, "Snapshot settings restored.");
}


/*
 * LOCK
 */

int gom_mixer_c_l(int c)
{
	if (gom_driver_C (c) > -1)
		return !gom_mixer_unlockmask[c];
	else
		return -1;
}

void gom_mixer_c_l_set(enum gom_info_types std_verb, int c, int lock)
{
	int old_lock;
	if (gom_driver_C (c) > -1)
	{
		old_lock = gom_mixer_c_l(c);
		gom_mixer_unlockmask[c] = !lock;

		gom_info(std_verb,
						 "Channel %d(%s) %socked (was %socked)%s.",
						 c, gom_driver_C_name(c, 0),
						 gom_mixer_c_l(c) ? "l" : "unl",
						 old_lock         ? "l" : "unl",
						 (gom_driver_c (c) < 0) ? " [though currently unavailable]" :
						 (gom_driver_c_c_len(c) == 1)  ? " [though currently mono]" : "");
	}
	else
	{
		gom_info(GOM_INFO_ERROR,
						 "Can't (un)lock channel %d(%s): Unexistent channel.",
						 c, gom_driver_C_name(c, 0));
	}
}

/* set all locks, gom's config of set_volumes */
void gom_mixer_l_set(enum gom_info_types std_verb, int lock)
{
	int c;
	for (c=gom_driver_C_first(); c > -1; c=gom_driver_C_next(c, 1))
		gom_mixer_c_l_set(std_verb+1, c, lock);

	gom_info(std_verb,
					 "All channels %socked.",
					 lock ? "l" : "unl");
}

/* Setting volumes: standard */
void gom_mixer_c_v_set(enum gom_info_types std_verb, int c, int volume)
{
	int cc;

	if (gom_driver_c (c) > -1)
	{
		for (cc=gom_driver_c_c_first(c); cc > -1; cc=gom_driver_c_c_next(c, cc, 1))
			gom_driver_c_c_v_set(std_verb+1, c, cc, volume);

		if (volume == gom_driver_c_c_v(c, gom_driver_c_c_first(c)))
			gom_info(std_verb,
							 "All volumes for channel %d(%s) set to %d.",
							 c, gom_driver_c_name(c, 0),
							 volume);
	}
	else
	{
		gom_info(GOM_INFO_ERROR,
						 "Can't set volumes on channel %d(%s): not available.",
						 c, gom_driver_C_name(c, 0));
	}
}

/* Setting volume: lock-sensible */
void gom_mixer_c_c_v_set_l(enum gom_info_types std_verb, int c, int cc, int volume)
{
	if (gom_mixer_c_l(c))
		gom_mixer_c_v_set(std_verb, c, volume);
	else
		gom_driver_c_c_v_set(std_verb, c, cc, volume);
}

void gom_mixer_c_r_set_single(enum gom_info_types std_verb, int c)
{
	int c2;

	/* setting current ON (will show an useful error msg on error, too) */
	gom_driver_c_r_set(std_verb, c, 1);

	if (gom_driver_c_r(c) == 1)
	{
		/* setting all others OFF */
		for (c2 = gom_driver_c_first(); c2 > -1; c2 = gom_driver_c_next(c2, 1))
		{
			if (c != c2 && gom_driver_c_r(c2) > -1)
				gom_driver_c_r_set(std_verb+2, c2, 0);
		}
	}
}
