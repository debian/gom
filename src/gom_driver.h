/* gom, Gom is nOt yet another (Audio) Mixer.
 *
 * Copyright (C) 1996-2004  Stephan Sürken <absurd@olurdix.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * gom_driver.h: interface to soundcard driver
 */

/*
 * INCLUDES
 */

/*
 * MACROS
 */

/* driver-known channels & channel channels (volumes) */
#define GOM_DRIVER_CHANNELS         SOUND_MIXER_NRDEVICES  /* = gom_driver_C_len()    */
#define GOM_DRIVER_CHANNEL_CHANNELS 2                      /* = gom_driver_C_C_len(c) */

/*
 * DECLARATIONS/DEFINTIONS
 */

/*
 * FUNCTION PROTOTYPES
 */

/*
 * THIS DRIVER'S ID
 */
char * gom_driver();


/*
 * _d: DEVICE
 */

char * gom_driver_d();
int gom_driver_d_opened();
int gom_driver_d_set(enum gom_info_types std_verb, char * device_file);


/*
 * _C: MIXER CHANNELS KNOWN TO THIS DRIVER
 */

int gom_driver_C (int c);
int gom_driver_C_first ();
int gom_driver_C_last ();
int gom_driver_C_next(int c, int jump);
int gom_driver_C_len ();
char * gom_driver_C_name (unsigned c, int label);

/*
 * _c: AVAILABLE CHANNELS (OF CURRENT MIXER)
 */

int gom_driver_c (int c);
int gom_driver_c_first ();
int gom_driver_c_last ();
int gom_driver_c_next (int c, int jump);
int gom_driver_c_len ();
char * gom_driver_c_name (int c, int label);

/*
 * _C_C: CHANNEL CHANNELS (OF CURRENT MIXER)
 */

int gom_driver_C_C (int c, int cc);
int gom_driver_C_C_first (int c);
int gom_driver_C_C_last (int c);
int gom_driver_C_C_next (int c, int cc, int jump);
int gom_driver_C_C_len (int c);

/*
 * _c_c: AVAILABLE CHANNEL CHANNELS (OF CURRENT MIXER)
 */

int gom_driver_c_c (int c, int cc);
int gom_driver_c_c_first (int c);
int gom_driver_c_c_last (int c);
int gom_driver_c_c_next (int c, int cc, int jump);
int gom_driver_c_c_len (int c);

/*
 * _c_c_v: VOLUMES
 */
int gom_driver_c_c_v_max ();
int gom_driver_c_c_v_range(int v);
int gom_driver_c_c_v (int c, int cc);
void gom_driver_c_c_v_set (enum gom_info_types std_verb, int c, int cc, int v);

/*
 * _c_r: RECORD
 */
int gom_driver_c_r (int c);
void gom_driver_c_r_set (enum gom_info_types std_verb, int c, int r);
