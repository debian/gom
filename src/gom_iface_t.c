/* gom, Gom is nOt yet another (Audio) Mixer.
 *
 * Copyright (C) 1996-2004  Stephan Sürken <absurd@olurdix.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * gom_iface_t.c: terminal gomii
 */

/*
 * INCLUDES
 */
#include "gom.h"
#if defined(HAVE_NCURSES_H)
#include <ncurses.h>
#else
#include <curses.h>
#endif

/*
 * MACROS
 */
#define GOM_IFACE_T_MAX_STRING_LEN 100

/*
 * A channel box (CBOX):
 *
 *        0123456789012345678901234567890123456789
 * Line0: o------ 0. Vol  ------------------o
 * Line1: | L  50 [**********----------] L- |
 * Line2: | R  50 [**********----------] Rx |
 * Line0: o------ 1. Bass ------------------o
 * Line0: o------ 0. Vol  ------------------o
 * Line1: | L  50 /-******-------------\ L+ |
 * Line2: | R  50 \*******-------------/ Rx |
 * Line0: o------ 1. Bass ------------------o
 */
#define GOM_IFACE_T_CBOX_HEIGHT 3
#define GOM_IFACE_T_CBOX_WIDTH 35
#define GOM_IFACE_T_CBOX_SLIDER_WIDTH 20
/*
 * A display (DISPLAY):
 *
 *                                GOM - The Generic OSS Mixer
 *                                 gom 0.20+ (1997 Mar 14+)
 * -- Page 1/2 ---------------------------/dev/mixer----------------------------------------
 *          o------ 0. Vol  ------------------o------ 5. Spkr ------------------o
 *          | L  50 [**********----------] L+ | L  75 [***************-----] Lx |
 *          | R  50 [**********----------] Rx | R  75 [xxxxxxxxxxxxxxxxxxxx] Rx |
 *          o------ 1. Bass ------------------o------ 6. Line ------------------o
 *          | L  75 [***************-----] L+ | L  75 [***************-----] L+ |
 *          | R  75 [***************-----] Rx | R  75 [***************-----] R- |
 *          o------ 2. Trebl------------------o------ 7. Mic  ------------------o
 *          | L  70 [**************------] L+ | L  24 [****----------------] Lx |
 *          | R  70 [**************------] Rx | R  24 [xxxxxxxxxxxxxxxxxxxx] R+ |
 *          o------ 3. Synth------------------o------ 8. CD   ------------------o
 *          | L  75 [***************-----] L+ | L  75 [***************-----] L+ |
 *          | R  75 [***************-----] R- | R  75 [***************-----] R- |
 *          o------ 4. Pcm  ------------------o------ 12. IGain-----------------o
 *          | L  75 [***************-----] L+ | L  75 [***************-----] L+ |
 *          | R  75 [***************-----] Rx | R  75 [***************-----] Rx |
 *          o---------------------------------o---------------------------------o
 * ------------------------------------------info-------------------------------------------
 * GOM INFO : Welcome to the all-ascii(tm) terminal gomii!
 * ------------------------------------------help-------------------------------------------
 *        MOVE  :<Up/Down><Tab>     LOCK  : kK  SAVE: sS  UPDATE: u<C-l> PAGE: <Space>
 *        VOLUME:<Left/Right>-+fmM  RECORD: a   GET : gG  Z-SHOT: zZ  EXIT: <Esc> xX
 *
 */
#define GOM_IFACE_T_TITLE_LINES 2
#define GOM_IFACE_T_INFO_LINES  3       /* +1 = one line needed for last channel box line */
#define GOM_IFACE_T_INFO_LINE   (LINES - 1)
#define GOM_IFACE_T_MASK_LINES  (GOM_IFACE_T_TITLE_LINES + GOM_IFACE_T_INFO_LINES)

#define GOM_IFACE_T_CBOXES_Y ((LINES - GOM_IFACE_T_MASK_LINES) / GOM_IFACE_T_CBOX_HEIGHT)
#define GOM_IFACE_T_CBOXES_X (COLS / GOM_IFACE_T_CBOX_WIDTH)
#define GOM_IFACE_T_CBOXES   (GOM_IFACE_T_CBOXES_X * GOM_IFACE_T_CBOXES_Y)

#define GOM_IFACE_T_RECOMENDED_COLS 75

/*
 * DECLARATIONS/DEFINTIONS
 */

int gom_iface_t_action_action;
int gom_iface_t_is_initialized = 0;
int gom_iface_t_current_page = 0;
int gom_iface_t_pages;
int gom_iface_t_info_line;

/*
 * FUNCTION PROTOTYPES
 */
void gom_iface_t_refresh();

/*****************/

/*
 * HELPS: they don't refresh themselves...
 */
void gom_iface_t_cntrprintw(int line, char * str)
{
	mvprintw(line, (COLS - strlen(str)) / 2, "%s", str);
}

void gom_iface_t_filline(int line, char ch)
{
	int i;
	for (i = 0; i < COLS; i++) mvprintw(line, i, "%c", ch);
}

void gom_iface_t_seperator_line(int line, char * text)
{
	attrset(A_BOLD);
	gom_iface_t_filline(line, '-');
	gom_iface_t_cntrprintw(line, text); attrset(A_NORMAL);
}

void gom_iface_t_clear_cboxes_space()
{
	unsigned c;
	for (c = GOM_IFACE_T_TITLE_LINES; c <= LINES - GOM_IFACE_T_INFO_LINES; c++)
		gom_iface_t_filline(c, ' ');
}

/*
 * Custom gom_info()
 */

void gom_iface_t_info(enum gom_info_types kind, char * fmt, va_list vargs)
{
	if (gom_info_scroll())
	{
		if (gom_iface_t_info_line > (LINES - 5))
		{
			gom_iface_t_seperator_line(LINES-2, "Press any key for more");
			getch();
			gom_iface_t_clear_cboxes_space();
			gom_iface_t_info_line = GOM_IFACE_T_TITLE_LINES+1;
		}
		else
		{
			++gom_iface_t_info_line;
		}
		mvprintw(gom_iface_t_info_line, 0, "");
	}
	else
	{
		/* clear line */
		gom_iface_t_filline(GOM_IFACE_T_INFO_LINE, ' ');
		mvprintw(GOM_IFACE_T_INFO_LINE, 0, "");
	}
	vwprintw(stdscr, fmt, vargs);
	refresh();
}

/*
 * Custom option arg
 */
char * gom_iface_t_get_option_arg(char * prompt)
{
	static char option_arg[100] = "";

	switch (gom_iface_t_action_action)
	{
	case 'r':
		snprintf(option_arg, 100, "%d", !gom_driver_c_r(gom_mixer_c_current()));
		break;

	case 'k':
		snprintf(option_arg, 100, "%d", !gom_mixer_c_l(gom_mixer_c_current()));
		break;

	default:
		gom_iface_t_filline(GOM_IFACE_T_INFO_LINE, ' ');
		mvprintw(GOM_IFACE_T_INFO_LINE, 0, "%s", prompt);

		/* stop auto-refresh */
		gom_gomii_refival_set(GOM_INFO_VERBOSE+1, gom_gomii_refival(), NULL);

		leaveok(stdscr, FALSE); /* activate cursor */
		echo();
		wgetnstr(stdscr, option_arg, 100);
		noecho();
		leaveok(stdscr, TRUE); /* inactivate cursor */

		/* restart auto-refresh */
		gom_gomii_refival_set(GOM_INFO_VERBOSE+1, gom_gomii_refival(), gom_iface_t_refresh);

		gom_iface_t_filline(GOM_IFACE_T_INFO_LINE, ' ');
		break;
	}
	return option_arg;
}

/*
 * DRAWING
 */

void gom_iface_t_draw_mask()
{
	/* Line 0 */
	attrset(A_BOLD);
	gom_iface_t_cntrprintw(0, GOM_TITLE);

	/* Line -2 */
	gom_iface_t_seperator_line(LINES-2, "Help: hH----Info: VwtT----Quit: q");
}

void gom_iface_t_pre_scroll()
{
	gom_info(GOM_INFO_NORMAL, "Terminal gomii text scroller");
	gom_info_scroll_set(1);

	/* stop auto-refresh */
	gom_gomii_refival_set(GOM_INFO_VERBOSE+1, gom_gomii_refival(), NULL);

	gom_iface_t_clear_cboxes_space();
	gom_iface_t_seperator_line(GOM_IFACE_T_TITLE_LINES-1, "Terminal gomii text scroller");
	gom_iface_t_info_line = GOM_IFACE_T_TITLE_LINES;
}

void gom_iface_t_post_scroll()
{
	gom_iface_t_seperator_line(LINES-2, "Press any key to continue"); getch();

	/* restart auto-refresh */
	gom_gomii_refival_set(GOM_INFO_VERBOSE+1, gom_gomii_refival(), gom_iface_t_refresh);
	gom_info_scroll_set(0);
	gom_info(GOM_INFO_NORMAL, "Back from scrolling...");
}

void gom_iface_t_draw_bline(int line, int col)
{
	mvprintw(line, col, "o---------------------------------o");
}

/* draw Line1 or Line2 */
void gom_iface_t_draw_sline(int c, int cc, int volume)
{
	int i, per_slider = (GOM_IFACE_T_CBOX_SLIDER_WIDTH * volume) / 100;

	attrset(A_NORMAL);
	printw("|");

	if ((cc == 0) || gom_driver_c_c_len(c) == 2)
	{
		if ((gom_mixer_c_current() == c) && ((gom_mixer_c_c_current() == cc) || gom_mixer_c_l(c) || !gom_driver_c_c_len(c) == 2))
			attrset(A_STANDOUT);
		printw(" %c ", cc == 0 ? 'L' : 'R');
		attrset(A_NORMAL);

		printw("%s%d %c",
					 volume < 100 ? volume < 10 ? "  " : " "  : "",
					 volume,
					 (gom_mixer_c_l(c) && gom_driver_c_c_len(c) == 2) ? ((cc == 0) ? '/' : '\\') : '[');
		for (i = 0; i < GOM_IFACE_T_CBOX_SLIDER_WIDTH; i++)
			printw("%c", cc == 1 && !gom_driver_c_c_len(c) == 2 ? 'x' : i < per_slider ? '*' : '-');
		attrset(A_NORMAL);
		printw("%c ", (gom_mixer_c_l(c) && gom_driver_c_c_len(c) == 2) ? ((cc == 0) ? '\\' : '/') : ']');
	}
	else
	{
		printw("                              ");
	}
	attrset(A_NORMAL);
	if (cc == 0)
		printw("%s |", (gom_driver_c_r(c) > -1) ? gom_driver_c_r(c) ? "R+" : "R-" : "  ");
	else
		printw("   |", gom_driver_c_c_len(c) >= 2 ? gom_mixer_c_l(c) ? '+' : '-' : 'x');
}

int gom_iface_t_c_number(int c)
{
	int c2;
	int no=0;
	for (c2 = gom_driver_c_first();
			 (c2 > -1) && (c2 != c);
			 c2 = gom_driver_c_next(c2, +1))
		no++;
	return no;
}

int gom_iface_t_cbox(int c)
{
	int no;
	if (gom_driver_c(c) > -1)
	{
		no = gom_iface_t_c_number(c) - gom_iface_t_current_page * GOM_IFACE_T_CBOXES;
		if (no < GOM_IFACE_T_CBOXES)
			return no;
	}
	return -1;            /* don't display this channel (now) */
}

int gom_iface_t_last_cdisplay()
{
	int c;
	for (c=gom_driver_C_len()-1; gom_iface_t_cbox(c) < 0; c--);
	return c;
}

int gom_iface_t_cdisplays()
{
	int max;
	max = gom_driver_c_len();
	if (gom_iface_t_current_page < 1)
		max = gom_driver_c_len();
	else
		max = gom_driver_c_len()-((gom_iface_t_current_page-1) * GOM_IFACE_T_CBOXES);

	if (max > GOM_IFACE_T_CBOXES)
		max = GOM_IFACE_T_CBOXES;
	return max;
}

void gom_iface_t_draw_channel(int c)
{
	int cbox, cdisplay_line, cdisplay_col, line, col;

	cbox = gom_iface_t_cbox(c);
	if (cbox < 0)
		return;

	/* some helps */
	cdisplay_col  = cbox / GOM_IFACE_T_CBOXES_Y;  /* cdisplay_col this channel is in */
	cdisplay_line = cbox % GOM_IFACE_T_CBOXES_Y;  /* cdisplay_line actually displayed */

	/* calculate line & col */
	line = 0 + GOM_IFACE_T_TITLE_LINES + (cdisplay_line * GOM_IFACE_T_CBOX_HEIGHT);
	col  = (COLS - (GOM_IFACE_T_CBOXES_X * GOM_IFACE_T_CBOX_WIDTH)) / 2      /* centering */
		+ (cdisplay_col  * (GOM_IFACE_T_CBOX_WIDTH-1));

	/* Line 0 */
	gom_iface_t_draw_bline(line, col);
	attrset(A_BOLD);
	mvprintw(line,
					 col + 7,
					 " %i. %s ", c, gom_driver_C_name(c, 1));
	attrset(A_NORMAL);

	/* Line 1 */
	move(++line, col);
	gom_iface_t_draw_sline(c, 0, gom_driver_c_c_v(c, 0));

	/* Line 2 */
	move(++line, col);
	gom_iface_t_draw_sline(c, 1, gom_driver_c_c_v(c, 1));

	/* Draw final line if this is the last channel per line */
	if ((((gom_iface_t_c_number(c)+1) % GOM_IFACE_T_CBOXES_Y) == 0) ||
			(gom_iface_t_c_number(c)+1 == gom_driver_c_len()))
		gom_iface_t_draw_bline(++line, col);

	refresh();
}


/*
 * %
 */

void gom_iface_t_init()
{
	static char * help_text[]=
		{
			"",
			"Help on terminal gomii",
			"----------------------",
			"",
			"  You can use all command line options (except the few command",
			"  line _only_ options) by typing the character that builds up",
			"  the standard POSIX-like one-character option.",
			"",
			"  Terminal gomii specials:",
			"",
			"    <Left/Right>-+   De/increase volume by 1.",
			"    j                Jump: increase volume by 1/4 of total",
			"                     (flipping back to zero).",
			"    <Up/Down><Tab>   Select channels (or left/right volumes inside",
			"                     unlocked channels).",
			"    <Space>          Goto next page.",
			"    <C-l>            Do a (ncurses) display reinitialisation (useful, for",
			"                     example, after resizing a xterm I am running in).",
			"    <Esc>qQ          Quit the terminal gomii.",
			NULL
		};

	initscr();             /* init stdscr */

	/* test maximum cols / lines we need */
	if ((COLS < GOM_IFACE_T_CBOX_WIDTH) || (LINES < GOM_IFACE_T_CBOX_HEIGHT + GOM_IFACE_T_MASK_LINES))
	{
		gom_info(GOM_INFO_ERROR,
						 "Your terminal is too small. I need at least %i columns (now %i) and %i lines (now %i).",
						 GOM_IFACE_T_CBOX_WIDTH, COLS, GOM_IFACE_T_CBOX_HEIGHT + GOM_IFACE_T_MASK_LINES, LINES);
		gom_info(GOM_INFO_ERROR,
						 "-- [RETURN] to continue anyway --");
		getchar();
	}
	if (COLS < GOM_IFACE_T_RECOMENDED_COLS)
	{
		gom_info(GOM_INFO_ERROR,
						 "Warning: Your terminal is quite narrow; there might be display inconveniences.");
		gom_info(GOM_INFO_ERROR,
						 "Note: It would be perfect if you had at least %i columns (now %i).",
						 GOM_IFACE_T_RECOMENDED_COLS, COLS);
		gom_info(GOM_INFO_ERROR,
						 "-- [RETURN] to continue anyway --");
		getchar();
	}

	/* init the terminal */
	keypad(stdscr, TRUE);  /* be able to read KEY_RIGHT and kind */
	cbreak();              /* show all chars immediately */
	noecho();              /* no echo ... */
	leaveok(stdscr, TRUE); /* leave cursor where it is... */
	timeout(-1);           /* wait indefinitely */

	/* start auto-refresh */
	gom_gomii_refival_set(GOM_INFO_VERBOSE+1, gom_gomii_refival(), gom_iface_t_refresh);

	gom_info_custom_set(gom_iface_t_info);
	gom_action_help_special_set(help_text);

	/* set number of gom_iface_t_pages for this terminal */
	gom_iface_t_pages = (gom_driver_c_len() / GOM_IFACE_T_CBOXES);
	if (gom_driver_c_len() % GOM_IFACE_T_CBOXES != 0)
		++gom_iface_t_pages;

	/* I guess this is not needed */
	/* clear(); */

	gom_iface_t_is_initialized = 1;
}

void gom_iface_t_closeup()
{
	if (gom_iface_t_is_initialized)
	{
		/* stop auto-refresh */
		gom_gomii_refival_set(GOM_INFO_VERBOSE+1, gom_gomii_refival(), NULL);

		gom_info_custom_set(NULL);
		gom_action_help_special_set(NULL);

		clear();     /* don't leave rubbish on the screen */
		refresh();   /* activate clear */
		endwin();    /* close stdscr */

		gom_iface_t_is_initialized = 0;
	}
}

void gom_iface_t_update(enum gom_gomii_refresh_stage stage)
{
	int c, old_page;

	if (gom_iface_t_is_initialized)
	{
		/* set the old & new current page */
		old_page = gom_iface_t_current_page;
		gom_iface_t_current_page = (gom_iface_t_c_number(gom_mixer_c_current()) / GOM_IFACE_T_CBOXES);
	}
	else
	{
		/* this is not really needed */
		old_page = (gom_iface_t_current_page = 0);
	}

	switch (stage)
	{
	case GOM_GOMII_REFRESH_INIT:
		/* Initiate terminal gomii */
		gom_iface_t_closeup();
		gom_iface_t_init();
		gom_info(GOM_INFO_NORMAL,
						 "Welcome to the all-ascii-no-color(tm) terminal gomii!");

	case GOM_GOMII_REFRESH_PAGE:
		/* mask, headline, clear space for all channel displays */
		gom_iface_t_draw_mask();
		gom_iface_t_seperator_line(GOM_IFACE_T_TITLE_LINES-1, gom_driver_d());
		gom_iface_t_clear_cboxes_space();

		/* Update page number */
		mvprintw(GOM_IFACE_T_TITLE_LINES-1, 1, " Page %i/%i ", 1+gom_iface_t_current_page, gom_iface_t_pages);

	case GOM_GOMII_REFRESH_CHANNELS:       /* the refresh routine starts here! */
		/* Update last refresh time */
		mvprintw(GOM_IFACE_T_TITLE_LINES-1,
						 COLS-strlen(gom_gomii_last_refresh_str())-3, " %s ",
						 gom_gomii_last_refresh_str());

		if (stage == GOM_GOMII_REFRESH_CHANNELS && old_page != gom_iface_t_current_page)
		{
			gom_iface_t_update(GOM_GOMII_REFRESH_PAGE); break;
		}
		else
		{
			/* redraw all channel displays */
			for (c = gom_driver_c_first(); c > -1; c = gom_driver_c_next(c, 1))
				gom_iface_t_draw_channel(c);
		}

	case GOM_GOMII_REFRESH_CURRENT_CHANNEL:
		if (stage == GOM_GOMII_REFRESH_CURRENT_CHANNEL && old_page != gom_iface_t_current_page)
		{
			gom_iface_t_update(GOM_GOMII_REFRESH_PAGE); break;
		}
		else if (stage == GOM_GOMII_REFRESH_CURRENT_CHANNEL)
		{
			/* we need to redraw _all_ channels to get the former current channel unmarked */
			gom_iface_t_update(GOM_GOMII_REFRESH_CHANNELS); break;
		}
		else
		{
			/* no action if called from above */
		}
		break;

	default:
		break;
	}
}

void gom_iface_t_refresh()
{
	gom_iface_t_update(GOM_GOMII_REFRESH_CHANNELS);
}

void gom_iface_t_add_volume(int add)
{
	int max = (add > 0 ? gom_driver_c_c_v_max() : 0);

	gom_mixer_c_c_v_set_l(GOM_INFO_NORMAL,
												gom_mixer_c_current(),
												gom_mixer_c_c_current(),
												gom_driver_c_c_v(gom_mixer_c_current(), gom_mixer_c_c_current()) == max ?
												max : gom_driver_c_c_v(gom_mixer_c_current(), gom_mixer_c_c_current())+add);
}


/* set new c and cc after moving UP(add=-1) or DOWN(add=+1) */
void gom_iface_t_upordown(int add)
{
	int new_c, new_cc;

	/* set c to current, cc to next(add) */
	new_c = gom_mixer_c_current();
	new_cc = gom_driver_c_c_next(new_c, gom_mixer_c_c_current(), add);

	if ((new_cc < 0) || gom_mixer_c_l(new_c))
	{
		new_c = gom_driver_c_next(gom_mixer_c_current(), add);
		if (new_c > -1)
		{
			if (add < 0)
				new_cc = gom_driver_c_c_last(new_c);
			else
				new_cc = gom_driver_c_c_first(new_c);
		}
		else
		{
			if (add < 0)
			{
				new_c = gom_driver_c_last();
				new_cc = gom_driver_c_c_last(new_c);
			}
			else
			{
				new_c = gom_driver_c_first();
				new_cc = gom_driver_c_c_first(new_c);
			}
		}
	}

	/* set the new values (there should be no errors) */
	gom_mixer_c_current_set(GOM_INFO_VERBOSE, new_c);
	gom_mixer_c_c_current_set(GOM_INFO_VERBOSE, new_cc);
}

void gom_iface_t(void)
{
	int jump_volume = 0;
	enum gom_gomii_refresh_stage stage = GOM_GOMII_REFRESH_INIT;

	do
	{
		/* do the overall update & re-set the default value for stage */
		gom_iface_t_update(stage);
		stage = GOM_GOMII_REFRESH_CURRENT_CHANNEL;

		/* get & evaluate the action char */
		switch (gom_iface_t_action_action = getch())
		{
		case 27:
		case 'q':
		case 'Q':
			gom_iface_t_closeup();
			return;

		case 32:
			/* SPACE */
			if (gom_iface_t_pages == 1)
				gom_info(GOM_INFO_NORMAL, "There is only one page.");
			else
			{
				/* set the new current channel */
				if (gom_driver_c_next(gom_iface_t_last_cdisplay(), +1) < 0)
					gom_mixer_c_current_set(GOM_INFO_VERBOSE, gom_driver_c_first());
				else
					gom_mixer_c_current_set(GOM_INFO_VERBOSE, gom_driver_c_next(gom_iface_t_last_cdisplay(), +1));
				/* set the new current volume channel */
				gom_mixer_c_c_current_set(GOM_INFO_VERBOSE, gom_driver_c_c_first(gom_mixer_c_current()));

				/* we have to refresh all channels for a new page */
				stage = GOM_GOMII_REFRESH_PAGE;
			}
			break;

		case KEY_UP:
			/* UP */
			gom_iface_t_upordown(-1);
			break;

		case 9:
		case KEY_DOWN:
			/* DOWN, TAB */
			gom_iface_t_upordown(+1);
			break;

			/* Volumes */
		case '+':
		case KEY_RIGHT:
			gom_iface_t_add_volume(+1);
			break;

		case '-':
		case KEY_LEFT:
			gom_iface_t_add_volume(-1);
			break;

			/* special: volume jumping */
		case 'j':
			jump_volume = (gom_driver_c_c_v(gom_mixer_c_current(), gom_mixer_c_c_current())
										 + (gom_driver_c_c_v_max()/4));
			jump_volume = jump_volume > gom_driver_c_c_v_max() ? 0 : jump_volume;
			gom_mixer_c_c_v_set_l(GOM_INFO_NORMAL,
														gom_mixer_c_current(), gom_mixer_c_c_current(), jump_volume);
			break;

		default:
			stage = gom_action(GOM_INFO_NORMAL,
												 gom_iface_t_action_action, 0, gom_iface_t_get_option_arg,
												 gom_iface_t_pre_scroll, gom_iface_t_post_scroll);
			break;
		}
	}
	while (1);

	gom_info(GOM_INFO_ERROR, "Bug: gom_iface_t()");
	return;
}
