/* gom, Gom is nOt yet another (Audio) Mixer.
 *
 * Copyright (C) 1996-2004  Stephan Sürken <absurd@olurdix.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * gom_gomii: gomii API
 */

/*
 * INCLUDES
 */

#include "gom.h"

/*
 * MACROS
 */

/*
 * DECLARATIONS/DEFINTIONS
 */

int gom_gomii_refival_var;      /* seconds */

/* helps for refresh handler procs */
RefProcFunc gom_gomii_refproc = 0;

/* void (*gom_gomii_refproc)() = NULL; */

/*
 * FUNCTION PROTOTYPES
 */

/*****************/


/*
 * GOMII REFRESH INTERVAL
 */

/* this proc actually gets installed as SIGALRM handler */
void gom_gomii_refresh_handler(int sig)
{
	if (gom_gomii_refproc != NULL)
	{
		gom_gomii_refproc();
	}
	else
		gom_info(GOM_INFO_ERROR,
						 "Bug: Gomii refresh handler called, but gom_gomii_refproc is NULL.");

	/* after signal is raised, sig-handler will be set to default (on most Un*xes, though not Linux) */
	/* So, we'd better renew this here */
	signal(SIGALRM, gom_gomii_refresh_handler);
}

/* use this dummy proc this proc actually gets installed as SIGALRM handler */
void gom_gomii_refproc_keep()
{}


int gom_gomii_refival()
{
  return gom_gomii_refival_var;
}

int gom_gomii_refival_set(enum gom_info_types std_verb, int interval, RefProcFunc refproc)
{
	struct itimerval real_timer;
	int err;
	int oldinterval = gom_gomii_refival();

	if ((interval >= GOM_GOMII_REFIVAL_MIN)
			&& (interval <= GOM_GOMII_REFIVAL_MAX))
	{
		/* set the new interval */
		gom_gomii_refival_var = interval;

		/* remove old timer, if exists */
		if (gom_gomii_refproc != NULL)
		{
			/* stop the real timer (this should always work) */
			real_timer.it_interval.tv_sec  = 0;
			real_timer.it_interval.tv_usec = 0;
			real_timer.it_value.tv_sec     = 0;
			real_timer.it_value.tv_usec    = 0;
			err = setitimer(ITIMER_REAL, &real_timer, NULL);
			if (err != 0)
				gom_info(GOM_INFO_ERROR,
								 "(ignored) Can't uninstall gomii auto refresh (\"setitimer\" failed with error code %d).", err);
			/* reset SIGALRM handler */
			signal(SIGALRM, SIG_DFL);
		}

		/* set the new refproc */
		if (refproc != gom_gomii_refproc_keep)
			gom_gomii_refproc = refproc;

		/* install new timer, if demanded */
		if (gom_gomii_refproc != NULL)
		{
			/* set the real timer */
			real_timer.it_interval.tv_sec  = gom_gomii_refival();
			real_timer.it_interval.tv_usec = 0;
			real_timer.it_value.tv_sec     = gom_gomii_refival();
			real_timer.it_value.tv_usec    = 0;
			err = setitimer(ITIMER_REAL, &real_timer, NULL);
			if (err != 0)
				gom_info(GOM_INFO_ERROR,
								 "Can't install gomii auto refresh: \"setitimer\" failed with error code %d.", err);
			else
				/* set the SIGALRM handler */
				signal(SIGALRM, gom_gomii_refresh_handler);
		}
		gom_info(std_verb,
						 "Gomii refresh interval set to %d %s (was %d %s).",
						 interval, (interval == 0 ? "(disabled)" : interval == 1 ? "second" : "seconds"),
						 oldinterval, (oldinterval == 0 ? "(disabled)" : oldinterval == 1 ? "second" : "seconds"));
	}
	else
		gom_info(GOM_INFO_ERROR,
						 "Requested gomii refresh interval (%d seconds) out of range (allowed range: %d <= interval <= %d).",
						 interval, GOM_GOMII_REFIVAL_MIN, GOM_GOMII_REFIVAL_MAX);

 return oldinterval;
}


/*
 * MISC
 */
char * gom_gomii_last_refresh_str()
{
	static char last_refresh_string[30];

	time_t current_time;
	current_time = time(NULL);
	strftime(last_refresh_string, 29, "Last refresh: %X", localtime(&current_time));
	return (char *) &last_refresh_string;
}
