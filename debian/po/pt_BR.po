# Debconf translations for gom
# Copyright (C) 2012 THE gom'S COPYRIGHT HOLDER
# This file is distributed under the same license as the gom package.
# Eder L. Marques <frolic@debian-ce.org>, 2007.
# José dos Santos Júnior <j.s.junior@live.com>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: gom 0.30.2-5.2\n"
"Report-Msgid-Bugs-To: gom@packages.debian.org\n"
"POT-Creation-Date: 2012-08-27 10:43-0400\n"
"PO-Revision-Date: 2012-08-30 19:30-0300\n"
"Last-Translator: J. S. Júnior <j.s.junior@live.com>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../gom.templates:1001
msgid "Initialize mixer on system startup?"
msgstr "Inicializar o mixer na inicialização do sistema?"

#. Type: boolean
#. Description
#: ../gom.templates:1001
msgid ""
"If you choose this option, \"/etc/init.d/gom start\" (on system startup, or "
"if run manually) will set mixer settings to your saved configuration."
msgstr ""
"Se você escolher esta opção, \"/etc/init.d/gom start\" (na inicialização do "
"sistema, ou se executar manualmente) definirá as configurações do mixer para "
"as suas configurações salvas."

#. Type: boolean
#. Description
#: ../gom.templates:1001
msgid ""
"You may use upstream's \"gomconfig\" script as root later to fine-tune the "
"settings."
msgstr ""
"Você pode usar o script \"gomconfig\" do autor original (\"upstream\") como "
"root, posteriormente, para ajustar os detalhes da configuração."

#. Type: string
#. Description
#: ../gom.templates:2001
msgid "Space-separated list of valid sound devices:"
msgstr "Lista dos dispositivos de som válidos separados por espaços:"

#. Type: string
#. Description
#: ../gom.templates:2001
msgid ""
"If none of these device names exists in /proc/devices, /etc/init.d/gom will "
"exit silently (so it does not produce errors if the system has no sound at "
"all)."
msgstr ""
"Se nenhum destes dispositivos existir em /proc/devices, /etc/init.d/gom "
"sairá silenciosamente (assim ele não produzirá erros se o seu sistema não "
"tiver som)."

#. Type: string
#. Description
#: ../gom.templates:2001
msgid ""
"You would usually not touch the default value (\"sound\" is OSS, \"alsa\" is "
"ALSA)."
msgstr ""
"Você geralmente não deveria tocar no valor padrão (\"sound\" é OSS, \"alsa\" "
"é ALSA)."

#. Type: boolean
#. Description
#: ../gom.templates:3001
msgid "Remove /etc/gom completely?"
msgstr "Remover o /etc/gom completamente?"

#. Type: boolean
#. Description
#: ../gom.templates:3001
msgid ""
"The /etc/gom directory seems to contain additional local customization "
"files. Please choose whether you want to remove it entirely."
msgstr ""
"O diretório /etc/gom parece conter arquivos locais personalizados. Por "
"favor, escolha se você quer removê-lo completamente."

#. Type: boolean
#. Description
#: ../gom.templates:4001
msgid "Remove obsoleted /etc/rc.boot/gom?"
msgstr "Remover o obsoleto /etc/rc.boot/gom?"

#. Type: boolean
#. Description
#: ../gom.templates:4001
msgid ""
"The /etc/rc.boot/gom file is obsoleted but might contain local "
"customizations."
msgstr ""
"O arquivo /etc/rc.boot/gom está obsoleto mas pode conter personalizações "
"locais."
